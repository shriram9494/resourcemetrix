import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { map } from "rxjs/operators";

@Injectable()
export class ApiService {
  constructor(private http: Http) {}

  getData(apiUrl: string, apiName: string, paramsdata: any = null) {
    return new Promise((resolve, reject) => {
      var params = paramsdata ? { filter: paramsdata } : null;
      if (!params) {
      }
      var headers = new Headers();
      this.http
        .get(apiUrl + "/" + apiName, { headers: headers, params })
        .pipe(map((data: any) => data.json()))
        .subscribe(
          data => {
            if (data) {
              resolve(data);
            }
          },
          err => {
            reject(err);
          }
        );
    });
  }

  postData(apiUrl: string, apiName: string, input: any) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      this.http
        .post(apiUrl + "/" + apiName, input, { headers: headers })
        .pipe(map((response: any) => response.json()))
        .subscribe(
          response => {
            //this.showSuccess("Details Saved Successfully.");
            resolve(response);
            //console.log("Record Created Successfully.");
          },
          err => {
            //this.showErr(err);
            reject(err);
          }
        );
    });
  }
}
