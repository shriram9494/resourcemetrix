import { Component } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-crypto-dashboard',
  templateUrl: './crypto-dashboard.component.html',
  styleUrls: ['./crypto-dashboard.component.scss']
})
export class CryptoDashboardComponent {
  donut:any={
    data:[]
  };
  constructor() {
      this.IPDOnut();
   }
   IPDOnut(){
    console.log("hi");
   this.donut = am4core.create("IPDonut", am4charts.PieChart);
   this.donut.data = [
    {
      country: "Lithuania",
      litres: 501.9
    },
    {
      country: "Czech Republic",
      litres: 301.9
    },
    {
      country: "Ireland",
      litres: 201.1
    },
    {
      country: "Germany",
      litres: 165.8
    },
    {
      country: "Australia",
      litres: 139.9
    },
    {
      country: "Austria",
      litres: 128.3
    },
    {
      country: "UK",
      litres: 99
    },
    {
      country: "Belgium",
      litres: 60
    },
    {
      country: "The Netherlands",
      litres: 50
    }
  ];
  
   am4core.useTheme(am4themes_animated);
   this.donut.innerRadius = am4core.percent(50);
   var series = this.donut.series.push(new am4charts.PieSeries());
   series.dataFields.value = "ip_count";
   series.dataFields.category = "ip";
   series.slices.template.cornerRadius = 10;
   series.slices.template.innerCornerRadius = 7;
 series.hiddenState.properties.opacity = 1;
 series.hiddenState.properties.endAngle = -90;
 series.hiddenState.properties.startAngle = -90;
 series.slices.template.events.on("hit", function clickSlice(value){
   },this);
   this.donut.legend = new am4charts.Legend();
 }
}
