import { CryptoDashboardComponent } from './crypto-dashboard/crypto-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { DashboardComponent } from './analytics/analytics.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { EmployeeComponent } from '../master/employee/employee.component';
import { DepartmentComponent } from '../master/department/department.component';
import { OrganizationComponent } from '../master/organization/organization.component';
import { ProjectdetailsComponent } from '../master/projectdetails/projectdetails.component';
import { ResourcecountComponent } from '../report/resourcecount/resourcecount.component';
import { ProjectassignComponent } from '../Process/projectassign/projectassign.component';
import { TechnologyComponent } from '../master/technology/technology.component';
import { DesigntionComponent } from '../master/designtion/designtion.component';
import { ProjectAssignReportComponent } from '../report/project-assign-report/project-assign-report.component';
import { UsersComponent } from '../master/users/users.component';
import { OemComponent } from '../master/oem/oem.component';
import { ProductComponent } from '../master/product/product.component';
import { ExpertiesComponent } from '../master/experties/experties.component';
import { RoleComponent } from '../master/role/role.component';
import { RolepermissionComponent } from '../User/rolepermission/rolepermission.component';
import { BenchemployeeComponent } from '../report/benchemployee/benchemployee.component';
import { EmployeeReportComponent } from '../report/employee-report/employee-report.component';
import { PipelineprojectComponent } from '../report/pipelineproject/pipelineproject.component';
import { HireComponent } from '../Process/hire/hire.component';
import { AssignedprojectComponent } from '../Process/assignedproject/assignedproject.component';
import { ClaimDocumentComponent } from '../Process/claim-document/claim-document.component';


const routes: Routes = [
  {
    path: 'analytics',
    component: DashboardComponent,
    data: {
      title: 'Analytics Dashboard'
    }
  },
  {
    path: 'ecommerce',
    component: EcommerceComponent,
    data: {
      title: 'E-Commerce Dashboard'
    }
  },
  {
    path: 'pipelineproject',
    component: PipelineprojectComponent,
    data: {
      title: 'Pipeline Project'
    }
  },

  {
    path: 'crypto',
    component: CryptoDashboardComponent,
    data: {
      title: 'CryptoCurrency'
    }
  },
  {
    path: 'project',
    component: ProjectDashboardComponent,
    data: {
      title: 'Project'
    }
  },

  {
    path: 'employee',
    component: EmployeeComponent,
    data: {
      title: 'Employee'
    }
  },

  {
    path: 'role',
    component: RoleComponent,
    data: {
      title: 'Role'
    }
  },

  {
    path: 'role-permission',
    component: RolepermissionComponent,
    data: {
      title: 'Role Permission'
    }
  },

  {
    path: 'department',
    component: DepartmentComponent,
    data: {
      title: 'Department'
    }
  },

  {
    path: 'oem',
    component: OemComponent,
    data: {
      title: 'OEM'
    }
  },

  {
    path: 'product',
    component: ProductComponent,
    data: {
      title: 'Product'
    }
  },

  {
    path: 'technology',
    component: TechnologyComponent,
    data: {
      title: 'Technology'
    }
  },


  {
    path: 'designation',
    component: DesigntionComponent,
    data: {
      title: 'Designation'
    }
  },

  {
    path: 'organization',
    component: OrganizationComponent,
    data: {
      title: 'Customer'
    }
  },
  {
    path: 'ProjectDetail',
    component: ProjectdetailsComponent,
    data: {
      title: 'Project Details'
    }
  },

  {
    path: 'experties',
    component: ExpertiesComponent,
    data: {
      title: 'Expertise'
    }
  },

  {
    path: 'MIS',
    component: ResourcecountComponent,
    data: {
      title: 'MIS'
    }
  },
  {
    path: 'prjAssignReport',
    component: ProjectAssignReportComponent,
    data: {
      title: 'Project Details'
    }
  },
  {
    path: 'usrAct',
    component: UsersComponent,
    data: {
      title: 'User Activation'
    }
  },
  {
    path: 'employeereport',
    component: EmployeeReportComponent,
    data: {
      title: 'EMPLOYEE PROJECT DETAILS'
    }
  },
  {
    path: 'benchemp',
    component: BenchemployeeComponent,
    data: {
      title: 'BENCH EMPLOYEE LIST'
    }
  },

  {
    path: 'projectAssign',
    component: ProjectassignComponent,
    data: {
      title: 'Project Assingment'
    }
  },

  {
    path: 'projectAssign/:user',
    component: ProjectassignComponent,
    data: {
      title: 'Project Assingment'
    }
  },

  {
    path: 'assignedproject',
    component: AssignedprojectComponent,
    data: {
      title: 'Assigned projects'
    }
  },

  {
    path: 'claimdocument',
    component: ClaimDocumentComponent,
    data: {
      title: 'Claims Documents '
    }
  },

  // {
  //   path: 'myprofile',
  //   component: MyprofileComponent,
  //   data: {
  //     title: 'My Profile'
  //   }
  // },

  // {
  //   path: 'projectAssign/:project',
  //   component: AssignedprojectComponent,
  //   data: {
  //     title: 'Project Assingment'
  //   }
  // },

  {
    path: 'hire',
    component: HireComponent,
    data: {
      title: 'Hire'
    }
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
