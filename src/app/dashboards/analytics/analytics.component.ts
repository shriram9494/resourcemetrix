import { Component,OnInit, NgZone, AfterViewInit } from '@angular/core';
import { AdalService } from 'adal-angular4';
import * as am4core from '@amcharts/amcharts4/core';
import { ApiService } from './../../service/api-service';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { environment } from '../../../environments/environment';
am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit  {


  ngAfterViewInit(): void {
    // this.IPDOnut();
  }
  test: boolean = false;
  ngOnInit(): void {
    setTimeout(() => {
        this.getEmployee();
    }, 1000);
  }
  constructor(private adalService: AdalService, private apiser: ApiService) {
 }
data: any = [];



getEmployee(): void {
  const companyId = sessionStorage.getItem('companyId');
  this.apiser.getData(environment.localurlms, 'Employeechartcout', companyId).then((t: any) => {
       this.data = t.recordsets[0];
       this.IPDOnut();
  });
}


 IPDOnut(){
  var chart = am4core.create('chartdiv', am4charts.PieChart3D);
  chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

  chart.legend = new am4charts.Legend();

  chart.data = this.data;

  var series = chart.series.push(new am4charts.PieSeries3D());
  series.dataFields.value = 'count1';
  series.dataFields.category = 'emp';

  }

}
