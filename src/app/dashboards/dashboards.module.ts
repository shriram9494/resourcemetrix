import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';

// Shared
import { SharedModule } from './../shared/shared.module';
import { routing } from './dashboards-routing.module';

// Dashboards
import { DashboardComponent } from './analytics/analytics.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { CryptoDashboardComponent } from './crypto-dashboard/crypto-dashboard.component';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';

// Components
import { TicketsWidgetComponent } from './analytics/components/tickets-widget/tickets-widget.component';
import { ChartsWidgetComponent } from './analytics/components/charts-widget/charts-widget.component';
import { MapsWidgetComponent } from './analytics/components/maps-widget/maps-widget.component';
import { TableWidgetComponent } from './analytics/components/table-widget/table-widget.component';
import { ContactsWidgetComponent } from './analytics/components/contacts-widget/contacts-widget.component';
import { TodoWidgetComponent } from './analytics/components/todo-widget/todo-widget.component';
import { NewsWidgetComponent } from './analytics/components/news-widget/news-widget.component';
import { WeatherWidgetComponent } from './analytics/components/weather-widget/weather-widget.component';
import { CalendarWidgetComponent } from './analytics/components/calendar-widget/calendar-widget.component';
import { TabbedChartWidgetComponent } from './analytics/components/tabbed-chart-widget/tabbed-chart-widget.component';
import { ActiveUsersComponent } from './analytics/components/active-users/active-users.component';
import { CommerceActiveUsersComponent } from './ecommerce/components/commerce-active-users/commerce-active-users.component';
import { RecentSalesComponent } from './ecommerce/components/recent-sales/recent-sales.component';
import { NewCustomersComponent } from './ecommerce/components/new-customers/new-customers.component';
import { DailySalesComponent } from './ecommerce/components/daily-sales/daily-sales.component';
import { ProductSalesComponent } from './ecommerce/components/product-sales/product-sales.component';
import { CommerceMapComponent } from './ecommerce/components/commerce-map/commerce-map.component';
import { DailyPerformanceComponent } from './crypto-dashboard/components/daily-performance/daily-performance.component';
import { AnnualPerformanceComponent } from './crypto-dashboard/components/annual-performance/annual-performance.component';
import { MostPopularComponent } from './crypto-dashboard/components/most-popular/most-popular.component';
import { MarketCapComponent } from './crypto-dashboard/components/market-cap/market-cap.component';
import { DoughnutChartWidgetComponent } from './analytics/components/doughnut-chart-widget/doughnut-chart-widget.component';
import { GdaxTickerComponent } from './crypto-dashboard/components/gdax-ticker/gdax-ticker.component';
import { WeeklyIssuesComponent } from './project-dashboard/components/weekly-issues/weekly-issues.component';
import { IssuesStatusComponent } from './project-dashboard/components/issues-status/issues-status.component';
import { BacklogDashboardComponent } from './project-dashboard/components/backlog/backlog.component';
import { UserTasksComponent } from './project-dashboard/components/user-tasks/user-tasks.component';
import { ProjectStatesComponent } from './project-dashboard/components/project-states/project-states.component';
import { EmployeeComponent } from '../master/employee/employee.component';
import { DepartmentComponent } from '../master/department/department.component';
import { OrganizationComponent } from '../master/organization/organization.component';
import { ProjectassignComponent } from '../Process/projectassign/projectassign.component';
import { ProjectAssignReportComponent } from '../report/project-assign-report/project-assign-report.component';


// Directives
import { CounterDirective } from './counter.directive';
import { ProjectdetailsComponent } from '../master/projectdetails/projectdetails.component';
import { AdddepartmentComponent } from '../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../shared/confirmdialog/confirmdialog.component';
import { ResourcecountComponent } from '../report/resourcecount/resourcecount.component';
import { FilterPipe } from '../pipe/filter.pipe';
// tslint:disable-next-line: import-destructuring-spacing
import {NgxPaginationModule} from 'ngx-pagination';
import { TechnologyComponent } from '../master/technology/technology.component';
import { DesigntionComponent } from '../master/designtion/designtion.component';
import { UsersComponent } from '../master/users/users.component';
import { OemComponent } from '../master/oem/oem.component';
import { ProductComponent } from '../master/product/product.component';
import { ExpertiesComponent } from '../master/experties/experties.component';
import { RoleComponent } from '../master/role/role.component';
import { AddrolepopupComponent } from '../shared/addrolepopup/addrolepopup.component';
import { RolepermissionComponent } from '../User/rolepermission/rolepermission.component';
import { BenchemployeeComponent } from '../report/benchemployee/benchemployee.component';
import { EmployeeReportComponent } from '../report/employee-report/employee-report.component';
import { PipelineprojectComponent } from '../report/pipelineproject/pipelineproject.component';
import { HireComponent } from '../Process/hire/hire.component';
import { AssignedprojectComponent } from '../Process/assignedproject/assignedproject.component';
import { ShowDetailsComponent } from '../shared/popup/show-details/show-details.component';
import { ClaimDocumentComponent } from '../Process/claim-document/claim-document.component';


@NgModule({
  imports: [
    routing,
    SharedModule,
    HttpClientModule,
    ChartsModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAUBMxehoelhfxTDaVxV2MIm6L1ptcJzpU'
    }),
  ],
  declarations: [
    DashboardComponent,
    EcommerceComponent,
    FilterPipe,
    TicketsWidgetComponent,
    ChartsWidgetComponent,
    MapsWidgetComponent,
    TableWidgetComponent,
    ContactsWidgetComponent,
    TodoWidgetComponent,
    NewsWidgetComponent,
    CounterDirective,
    WeatherWidgetComponent,
    CalendarWidgetComponent,
    TabbedChartWidgetComponent,
    ActiveUsersComponent,
    CommerceActiveUsersComponent,
    RecentSalesComponent,
    NewCustomersComponent,
    DailySalesComponent,
    ProductSalesComponent,
    CommerceMapComponent,
    DailyPerformanceComponent,
    AnnualPerformanceComponent,
    DoughnutChartWidgetComponent,
    CryptoDashboardComponent,
    ProjectDashboardComponent,
    MostPopularComponent,
    MarketCapComponent,
    GdaxTickerComponent,
    WeeklyIssuesComponent,
    IssuesStatusComponent,
    BacklogDashboardComponent,
    UserTasksComponent,
    ProjectStatesComponent,
    EmployeeComponent,
    DepartmentComponent,
    OrganizationComponent,
    ProjectdetailsComponent,
    AdddepartmentComponent,
    ConfirmdialogComponent,
    ResourcecountComponent,
    ProjectassignComponent,
    TechnologyComponent,
    DesigntionComponent,
    ProjectAssignReportComponent,
    UsersComponent,
    OemComponent,
    ProductComponent,
    ExpertiesComponent,
    RoleComponent,
    AddrolepopupComponent,
    RolepermissionComponent,
    BenchemployeeComponent,
    EmployeeReportComponent,
    PipelineprojectComponent,
    HireComponent,
    AssignedprojectComponent,
    ShowDetailsComponent,
    ClaimDocumentComponent

  ],
  entryComponents: [ AdddepartmentComponent, ConfirmdialogComponent, AddrolepopupComponent, ShowDetailsComponent]
})
export class DashboardsModule { }
