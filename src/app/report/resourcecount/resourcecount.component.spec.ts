import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcecountComponent } from './resourcecount.component';

describe('ResourcecountComponent', () => {
  let component: ResourcecountComponent;
  let fixture: ComponentFixture<ResourcecountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourcecountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcecountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
