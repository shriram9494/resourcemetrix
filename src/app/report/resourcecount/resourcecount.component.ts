import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-resourcecount',
  templateUrl: './resourcecount.component.html',
  styleUrls: ['./resourcecount.component.scss'],
  providers: [
    { provide: ApiService, useClass: ApiService}
  ],
})
export class ResourcecountComponent implements OnInit {
  p: number = 1;
  tableshowbusy:boolean=false;
  tableshowAvail:boolean=false;
  employeebusy:any=[];
  employeeAvail:any=[];
  technology: any;
  level: any;
  constructor(private apiser:ApiService) { }
  resourceCout:any=[];
  ngOnInit() {
    this.getData();
  }

  getData(){
    const obj: any = {};
    obj.companyId = sessionStorage.getItem('companyId');
    this.apiser.getData(environment.localurlms, 'MIS', obj).then((t: any) => {
        this.resourceCout = t.recordset;
        console.log(this.resourceCout);
    });

  }

  busycnt(det: any): void {
    this.tableshowbusy = true;
    this.tableshowAvail = false;
    det.companyId = sessionStorage.getItem('companyId');
    console.log(det);
    this.apiser.getData(environment.localurlms,'busyresouce', det).then((t: any) => {
       this.employeebusy = t.recordset;
      // console.log(t.recordset);
  });

}
availcnt(det: any): void {
    this.tableshowAvail = true;
    this.tableshowbusy = false;
    det.companyId = sessionStorage.getItem('companyId');
    console.log(det);
    this.apiser.getData(environment.localurlms, 'availresouce', det).then((t: any) => {
       this.employeeAvail = t.recordset;
      // console.log(t.recordset);
  });
 }
}
