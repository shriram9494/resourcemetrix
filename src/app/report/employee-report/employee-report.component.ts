import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material';
import { ShowDetailsComponent } from '../../shared/popup/show-details/show-details.component';

@Component({
  selector: 'app-employee-report',
  templateUrl: './employee-report.component.html',
  styleUrls: ['./employee-report.component.scss'],
  providers: [
    { provide: ApiService, useClass: ApiService}
  ]
})
export class EmployeeReportComponent implements OnInit {
emp: any;
p: any;
  constructor(private apiser: ApiService, private dialog: MatDialog) { }
  projectList: any = [];

  ngOnInit(): void {
    this.getData();
  }
  getData(): void {
    const obj: any = {};
    obj.companyId = sessionStorage.getItem('companyId');
    obj.empId = sessionStorage.getItem('usrEmpId');
    this.apiser.getData(environment.localurlms, 'employeeProject', obj).then((t: any) => {
        this.projectList = t.recordset;
        console.log(this.projectList);
    });
  }

  openDialogDetails(data: any): void {
    const product = data;
    console.log(product);

    const dialogName = 'Details';
    const diaglogRef = this.dialog.open(ShowDetailsComponent, {
      data: { product, dialogName }
    });

    // diaglogRef.componentInstance.onAdd.subscribe((response) => {

    // });


  }



}
