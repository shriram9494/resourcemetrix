import { Technology } from './../../master/employee/employee.component';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pipelineproject',
  templateUrl: './pipelineproject.component.html',
  styleUrls: ['./pipelineproject.component.scss']
})
export class PipelineprojectComponent implements OnInit {

  p: number = 1;
  orgname: any;
  prjname: any;
  deptname: any;
  contact: any;
  email: any;
  city: any;
  state: any;
  country: any;
  technology: any;
  level: any;
  tableDetails: boolean = false;
  pipelineproject: any = [];

  constructor(private apiser: ApiService) { }

  ngOnInit() {
    let obj:any={};
    obj.companyId=sessionStorage.getItem("companyId");
    this.apiser.getData(environment.localurlms,"getpipelineproject",obj).then((t:any)=> {
        this.pipelineproject=t.recordset;
        console.log(this.pipelineproject);
    });

  }

}
