import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipelineprojectComponent } from './pipelineproject.component';

describe('PipelineprojectComponent', () => {
  let component: PipelineprojectComponent;
  let fixture: ComponentFixture<PipelineprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipelineprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
