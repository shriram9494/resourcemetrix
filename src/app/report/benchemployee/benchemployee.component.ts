import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-benchemployee',
  templateUrl: './benchemployee.component.html',
  styleUrls: ['./benchemployee.component.scss'],
  providers: [
    { provide: ApiService, useClass: ApiService}
  ]
})
export class BenchemployeeComponent implements OnInit {

  EmployeeList: any = [];
  emp: any;
  email: any;
  cont: any;
  tech: any;
  compt: any;
  level: any;
  p: any;

  constructor(private apiser: ApiService) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    let obj:any={};
    obj.companyId=sessionStorage.getItem("companyId");
    this.apiser.getData(environment.localurlms,"benchEmployee",obj).then((t:any)=> {
        this.EmployeeList=t.recordset;
        console.log(this.EmployeeList);
    });
  }

}
