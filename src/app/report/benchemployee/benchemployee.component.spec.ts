import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenchemployeeComponent } from './benchemployee.component';

describe('BenchemployeeComponent', () => {
  let component: BenchemployeeComponent;
  let fixture: ComponentFixture<BenchemployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenchemployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenchemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
