import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectAssignReportComponent } from './project-assign-report.component';

describe('ProjectAssignReportComponent', () => {
  let component: ProjectAssignReportComponent;
  let fixture: ComponentFixture<ProjectAssignReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectAssignReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectAssignReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
