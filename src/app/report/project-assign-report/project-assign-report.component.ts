import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-project-assign-report',
  templateUrl: './project-assign-report.component.html',
  styleUrls: ['./project-assign-report.component.scss']
})
export class ProjectAssignReportComponent implements OnInit {

  p: number = 1;
  tableshowbusy: boolean = false;
  tableshowAvail: boolean = false;
  employeebusy: any = [];
  employeeAvail: any = [];
  Organization: any;
  project: any;
  technology: any;
  level: any;
  emp: any;
  employee: any;

  constructor(private apiser: ApiService) { }
  projectdet: any = [];
  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    let companyId=sessionStorage.getItem("companyId");
    this.apiser.getData(environment.localurlms,"prjAssignDetails",companyId).then((t:any)=> {
        this.projectdet = t.recordset;
        //console.log(this.projectdet);
    });
  }

}
