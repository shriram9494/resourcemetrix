import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdalService } from 'adal-angular4';
import swal from 'sweetalert';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'portal-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [
    { provide: ApiService, useClass: ApiService}
  ]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router, private adalService: AdalService,
    private apiser: ApiService
  ) { }
  userdata: any = {};
  textdisable: boolean = false;

  ngOnInit(): void {
    this.adalService.handleWindowCallback();
    this.loginForm = this.formBuilder.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required],
      'logTyp': ['', Validators.required]
    });
    //console.log(this.adalService.userInfo.authenticated);
    if (this.adalService.userInfo.authenticated==true) {
      let usrdet: any = {};
      usrdet.fname = this.adalService.userInfo.profile.given_name;
      usrdet.lname = this.adalService.userInfo.profile.family_name;
      usrdet.username = this.adalService.userInfo.profile.unique_name;
      usrdet.email = this.adalService.userInfo.profile.unique_name;
     // console.log(usrdet);
     sessionStorage.setItem('logintyp', 'ofc365');
      this.apiser.getData(environment.localurlms, 'userAvailable', usrdet).then((t: any) => {
        console.log(t);
        if (t.recordset[0].usr_IsActive == 1) {
          //console.log(t.recordset[0].usr_ID);
          sessionStorage.setItem('usrID', t.recordset[0].usr_ID);
          sessionStorage.setItem('usrEmpId', t.recordset[0].usr_empId);
          sessionStorage.setItem('companyId', '1');
          sessionStorage.setItem('roleId', t.recordset[0].usr_Role_Id);
            usrdet={};
            this.router.navigate(['/dashboards/analytics']);
        } else if (t.recordset[0].usr_empId == '' || t.recordset[0].usr_empId == null || t.recordset[0].usr_empId ==''){
          swal('Error', 'Contact Administrator for Add Your Employee Details', 'error');
        } else {
          swal('', 'Successfully Signup!! Contact Administrator for Active Account', 'success');
        }
      });
     // this.router.navigate(['/dashboards/analytics']);
    }
  }

  ofc365() {
      this.userdata.username = '';
      this.userdata.password = '';
      this.textdisable = true;
  }

  ldap(){
      this.textdisable = false;
  }


  login(): void {
     const dat = this.loginForm.value;
    if(this.textdisable==true){
        this.adalService.login();
        sessionStorage.setItem('logintyp', 'ofc365');
      }else{
        this.apiser.postData(environment.localurl, 'ldapauth', dat).then((t: any) => {
          if (t.login == true){
              this.apiser.getData(environment.localurlms, 'userAvailable', dat).then((t: any) => {
                if (t.recordset[0].usr_IsActive == 1) {
                  sessionStorage.setItem('usrID', t.recordset[0].usr_ID);
                  sessionStorage.setItem('logintyp', 'ldap');
                   this.router.navigate(['/dashboards/analytics']);
                  } else {
                    swal('', 'Successfully Signup!! Contact Administrator for Active Account', 'success');
                  }
              });
          } else {
            swal('', 'INCORRECT USRNAME & PASSWORD', 'error');
          }
         });
      }
  }

  register(): void {
    this.router.navigate(['/external/register']);
  }

  forgotPassword(): void {
    // this.router.navigate(['/external/forgot-password']);
  }

}
