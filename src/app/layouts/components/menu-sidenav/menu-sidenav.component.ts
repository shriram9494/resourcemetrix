import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Router, RouterEvent, NavigationStart } from '@angular/router';
import { Component, OnDestroy, Input } from '@angular/core';
import { MatSidenav } from '@angular/material';
import swal from 'sweetalert';
import { ApiService } from '../../../service/api-service';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'portal-menu-sidenav',
  templateUrl: './menu-sidenav.component.html',
  styleUrls: ['./menu-sidenav.component.scss'],
})
export class MenuSidenavComponent implements OnDestroy {
  /**
   * Import material sidenav so we can access open close functions.
   */
  @Input() sidenav: MatSidenav;
  routerSubscription: Subscription;
  dashboardata: any = [];
  grouparray: any = [];
  constructor(private router: Router,
    private apiser: ApiService) {
    this.routerSubscription = this.router.events
      .pipe(
        filter(event => event instanceof NavigationStart),
      )
      .subscribe((event: RouterEvent) => {
        if (this.sidenav && this.sidenav.mode === 'over' && this.sidenav.opened) {
          this.sidenav.close();
        }
      });
      this.getUsrRights();
  }

  getUsrRights(): void {
    let usrRoleDet: any = {};
    usrRoleDet.roleId = sessionStorage.getItem('roleId');
    usrRoleDet.companyId = sessionStorage.getItem('companyId');
    console.log(usrRoleDet);
    this.apiser.getData(environment.localurlms, 'RoleDetails', usrRoleDet).then((t: any) => {
          this.dashboardata = t.recordset;
          console.log('this.dashboardata',  this.dashboardata);
          this.getFormGroup();
          usrRoleDet = {};
      });
  }

  getFormGroup(){
    var flags = [], output = [], l = this.dashboardata.length, i;
    for( i=0; i<l; i++) {
        if( flags[this.dashboardata[i].FormGroup]) continue;
        flags[this.dashboardata[i].FormGroup] = true;
        let data:any={};
        data.fromGroup=this.dashboardata[i].FormGroup;
        data.icon= this.dashboardata[i].FormGroupIcon;
        this.grouparray.push(data);
    }
    console.log(this.grouparray);
     console.log('grouparray',  this.grouparray);
}

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }
}
