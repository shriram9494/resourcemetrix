import { Component, OnInit, Input } from '@angular/core';
import { AdalService } from 'adal-angular4';
import { ApiService } from '../../../service/api-service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { ProfileComponent } from '../../../pages/profile/profile.component';



@Component({
  selector: 'portal-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  providers: [
    { provide: ApiService, useClass: ApiService}
  ]
})
export class UserMenuComponent implements OnInit {

  usrdet: any = {};
  uid: number;
  userId: any;
  constructor(private apiser: ApiService, private adalService: AdalService,private router: Router, private api: ApiService) { }

  ngOnInit(): void {
    this.userId=sessionStorage.getItem('usrID');
    this.uid = this.userId;
    this.getUsr();
  }
  getUsr(){
    this.apiser.getData(environment.localurlms, "usrDetails", this.uid).then((t:any) => {
      console.log(t.recordset[0]);
      this.usrdet = t.recordset[0];
  });
  }
  signout(){
    let logtyp=sessionStorage.getItem('logintyp');
    if(logtyp=='ofc365'){
      sessionStorage.clear();
      this.adalService.logOut();
      this.router.navigate(['/login']);
    }else{
      sessionStorage.clear();
      this.router.navigate(['/login']);
    }
  }

  profile(): void{
    this.router.navigate(['/profile']);
  }

}
