import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import { BehaviorSubject, Observable } from "rxjs";
export class CustomDataSource implements DataSource<any> {
  [x: string]: any;
    private lessonsSubject = new BehaviorSubject<any[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    constructor(
    ){

    }
    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.lessonsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer){
        this.lessonsSubject.complete();
        this.loadingSubject.complete();
    }

    loadData(arr) {
        this.lessonsSubject.next(arr);
    }
}
