import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrolepopupComponent } from './addrolepopup.component';

describe('AddrolepopupComponent', () => {
  let component: AddrolepopupComponent;
  let fixture: ComponentFixture<AddrolepopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddrolepopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrolepopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
