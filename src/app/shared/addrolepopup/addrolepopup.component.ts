import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { ApiService } from '../../service/api-service';
import swal from 'sweetalert';


@Component({
  selector: 'app-addrolepopup',
  templateUrl: './addrolepopup.component.html',
  styleUrls: ['./addrolepopup.component.scss']
})
export class AddrolepopupComponent implements OnInit {

// tslint:disable-next-line: typedef
  roleForm: FormGroup;
  cardName: any;
  rollData: any = [];
  onAdd = new EventEmitter();
  userID: any;

  updateID: number = 0;
  constructor(public dialogRef: MatDialogRef<AddrolepopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private apiser: ApiService) {
      this.cardName = data.dialogName;
      this.rollData = this.data.roll;
      console.log('rollData=>>', this.rollData);

      this.userID = JSON.parse(sessionStorage.getItem('usrID'));


    }

  ngOnInit(): void {

    if (this.rollData) {
      this.setValue();
      this.updateID = this.rollData.RoleId;
    } else {
      this.createRoleForm();
     }
    }

  setValue(): void {
    this.roleForm = this.fb.group({
      rolename: [this.rollData.RoleName, Validators.required],
      roledesc: [this.rollData.RoleDescription, Validators.required]
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  createRoleForm(){
    this.roleForm = this.fb.group({
      rolename: ['', Validators.required],
      roledesc: ['', Validators.required]
    });
  }

  submitRole(): void {

    const formData = {
      name: this.roleForm.get('rolename').value,
      desc: this.roleForm.get('roledesc').value,
      uid: this.userID
    }

    this.apiser
    .postData(environment.localurlms, 'InsertRole', formData)
    .then((data: any) => {
      if (data.rowsAffected.length > 0) {
        swal('', 'Role Added Successfully !', 'success');
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });

  }

  updateRole(data): void {

    const formData = {
      name: this.roleForm.get('rolename').value,
      desc: this.roleForm.get('roledesc').value,
      uid: this.userID,
      id: this.rollData.RoleId
    };


    console.log('formData =>>', formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateRole', formData)
    .then((data: any) => {
      if (data.rowsAffected.length > 0) {
        swal('', 'Role Update Successfully !', 'success');
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });

  }

}
