
import { Employee } from './../model/employee';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map'

import { HttpModule, Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Technology } from '../../master/employee/employee.component';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: Http) {

   }

// tslint:disable-next-line: typedef
   addEmployee(employee: Employee) {
    return this.http.post(`${environment.localurlms}EmployeeInsert`, employee);
  }

// tslint:disable-next-line: typedef
  addTechnology(technology: Technology){
    return this.http.post(`${environment.localurlms}`, technology);
  }


}
