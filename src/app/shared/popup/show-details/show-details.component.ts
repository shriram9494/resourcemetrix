import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class ShowDetailsComponent implements OnInit {

  data: any = [];
  dialogName: any;
  projectName: any;
  showForm: FormGroup;
  constructor(public dialogRef: MatDialogRef<ShowDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public d: any, private fb: FormBuilder) {
          this.data = d;
          console.log('Edit data =>>', this.data);
        this.dialogName = this.data.dialogName;
       console.log(',.,.,',  this.data.product.PRJ_Name);

  }

  ngOnInit(): void {
    this.showForm = this.fb.group({
      PRJ_Name: [this.data.product.PRJ_Name],
      Project_Manager: [this.data.product.Project_Manager],
      State_Name: [this.data.product.State_Name],
      Technical_Manager: [this.data.product.Technical_Manager],
      city: [this.data.product.city],
      contact: [this.data.product.contact],
      country: [this.data.product.country],
      email: [this.data.product.email],
      projdescription: [this.data.product.projdescription],
      frm: [this.data.product.frm],
      tod: [this.data.product.tod],
    });

  }

  close(): void {
    this.dialogRef.close();
  }

}
