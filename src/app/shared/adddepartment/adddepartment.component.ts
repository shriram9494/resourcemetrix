import { User } from './../../dashboards/project-dashboard/components/user-tasks/user-tasks.component';
import { Position } from './../../master/employee/employee.component';
import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { ApiService } from '../../service/api-service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import swal from 'sweetalert';
import { CustomDataSource } from '../model/data-source';

@Component({
  selector: 'app-adddepartment',
  templateUrl: './adddepartment.component.html',
  styleUrls: ['./adddepartment.component.scss'],

})
export class AdddepartmentComponent implements OnInit {


  departmentForm: FormGroup;
  expertiesForm: FormGroup;
  designationForm: FormGroup;
  oemForm: FormGroup;
  productForm: FormGroup;
  oemData: any;
  data: any;
  userID: any;
  DEPT_ID: any;
  onAdd = new EventEmitter();
  dialogName: string;
  dialogId: number;
  oem_ID: any;
  company_ID: any;

  constructor(public dialogRef: MatDialogRef<AdddepartmentComponent>,
    @Inject(MAT_DIALOG_DATA) public d: any,
      private fb: FormBuilder, private apiser: ApiService ) {
          this.data = d;
          console.log("Edit data =>>", this.data);

        this.DEPT_ID = this.data.DEPT_ID;
        this.dialogName = this.data.dialogName;
        this.dialogId = this.data.dialogId;

  }

  ngOnInit(): void {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    console.log('sesstion id =>>>>', this.userID);
    this.company_ID = sessionStorage.getItem('companyId');

    this.createdepartForm();
    this.createExpertiesForm();
    this.createDesignationForm();
    this.createOemForm();
    this.createProductForm();
    this.getAllOEMDetails();

    if(this.data.user) {
      this.setValueData();
    }else{

    }
  }



  getAllOEMDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getOEM')
      .then((data: any) => {
        console.log('getOEM data =>>>>', data.recordset);
        this.oemData = data.recordset;
      });
  }


  setValueData(): void {

    this.productForm.controls['oem_id'].setValue(this.data.user.OEM_ID, { onlySelf: false });
    this.productForm.controls['productdesc'].setValue(this.data.user.PRODUCT_Description, { onlySelf: true });
    this.productForm.controls['productname'].setValue(this.data.user.PRODUCT_Name, { onlySelf: true });

    // OEM form set value field
    this.oemForm.controls['oname'].setValue(this.data.user.OEM_Name, { onlySelf: true });
    this.oemForm.controls['odesc'].setValue(this.data.user.OEM_Description, { onlySelf: true });

    // Technology form set value field
    this.designationForm.controls['designame'].setValue(this.data.user.DES_Name, { onlySelf: true });
    this.designationForm.controls['desigdesc'].setValue(this.data.user.DES_Desc, { onlySelf: true });

    // Department form set value field
    this.departmentForm.controls['dname'].setValue(this.data.user.DEPT_Name, { onlySelf: true });
    this.departmentForm.controls['ddesc'].setValue(this.data.user.DEPT_Desc, { onlySelf: true });

    // Technology form set value field
    this.expertiesForm.controls['exname'].setValue(this.data.user.COMPETENCE_Name, { onlySelf: true });
    this.expertiesForm.controls['exdesc'].setValue(this.data.user.COMPETENCE_Description, { onlySelf: true });

  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  createdepartForm(): void {
    this.departmentForm = this.fb.group({
      dname: ['', Validators.required],
      ddesc: ['', Validators.required]
    });
  }

  createExpertiesForm(): void {
    this.expertiesForm = this.fb.group({
      exname: ['', Validators.required],
      exdesc: ['', Validators.required]
    });
  }

  createDesignationForm(): void {
    this.designationForm = this.fb.group({
      designame: ['', Validators.required],
      desigdesc: ['', Validators.required]
    });
  }

  createOemForm(): void {
    this.oemForm = this.fb.group({
      oname: ['', Validators.required],
      odesc: ['', Validators.required]
    });
  }

  createProductForm(): void {
    this.productForm = this.fb.group({
      productname: ['', Validators.required],
      oem_id: ['', Validators.required],
      productdesc: ['', Validators.required]
    });
  }

  onOemSelection(value): void {
    this.oem_ID = value;
    console.log(this.oem_ID);

  }

  // All Insert function

  submitDept(value): void {
    const formData = {
      name: this.departmentForm.get('dname').value,
      desc: this.departmentForm.get('ddesc').value,
      uid: this.userID,
      companyId: this.company_ID
    }
    this.apiser
    .postData(environment.localurlms, 'InsertDepartment', formData)
    .then((data: any) => {
      if (data.rowsAffected.length > 0) {
        swal('', 'Department Added Successfully !', 'success');
        this.createdepartForm();
        this.createExpertiesForm();
        this.createDesignationForm();
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });
  }

  submitExperties(): void {
    const formData = {
      name: this.expertiesForm.get('exname').value,
      desc: this.expertiesForm.get('exdesc').value,
      uid: this.userID,
      companyId: this.company_ID
    };
    console.log('FormData =>>', formData);

    this.apiser
    .postData(environment.localurlms, 'InsertExperties', formData)
    .then((data: any) => {
      console.log('Expertie added =>>', data.recordset);

      if (data.rowsAffected.length > 0) {
        swal('', 'Experties Added Successfully !', 'success');
        this.createdepartForm();
        this.createExpertiesForm();
        this.createDesignationForm();
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });
  }


  submitOem(): void {
    const formData = {
      name: this.oemForm.get('oname').value,
      desc: this.oemForm.get('odesc').value,
      uid: this.userID,
      companyId: this.company_ID
    };
    console.log('FormData =>>', formData);

    this.apiser
    .postData(environment.localurlms, 'InsertOem', formData)
    .then((data: any) => {
      console.log('Oem added =>>', data.recordset);

      if (data.rowsAffected.length > 0) {
        swal('', 'OEM Added Successfully !', 'success');
        this.createdepartForm();
        this.createExpertiesForm();
        this.createDesignationForm();
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });
  }


  submitProduct(value): void {

  console.log(value);
  if(value.productname=='' || value.productdesc=='' || value.oemid=='' || this.userID==''){
    swal('', 'Please Insert the Product Details !', 'error');
    return;
  }

    const formData = {

      name: this.productForm.get('productname').value,
      desc: this.productForm.get('productdesc').value,
      oem_id: this.oem_ID,
      uid: this.userID,
      // companyId: this.company_ID
       // oem_id: this.productForm.get('oemid').value,

    };
    console.log('FormData =>>', formData);

    this.apiser
    .postData(environment.localurlms, 'InsertProduct', formData)
    .then((data: any) => {
      console.log('Product added =>>', data.recordset);

      if (data.rowsAffected.length > 0) {
        swal('', 'Product Added Successfully !', 'success');
        this.createdepartForm();
        this.createExpertiesForm();
        this.createDesignationForm();
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });
  }


  submitDesign(): void {
    const formData = {
      name: this.designationForm.get('designame').value,
      desc: this.designationForm.get('desigdesc').value,
      uid: this.userID,
      companyId: this.company_ID
    }
    this.apiser
    .postData(environment.localurlms, 'InsertDesignation', formData)
    .then((data: any) => {
      if (data.rowsAffected.length > 0) {
        swal('', 'Designation Added Successfully !', 'success');
        this.createdepartForm();
        this.createExpertiesForm();
        this.createDesignationForm();
        this.closeDialog();
        this.onAdd.emit();
      } else {
        swal('error');
        this.closeDialog();
      }
    });
  }


  // All Update Operation

  updateDept(value): void {
    const formData = {
      uid: this.userID,
      name: this.departmentForm.get('dname').value,
      desc: this.departmentForm.get('ddesc').value,
      id: this.data.user.DEPT_ID,
      companyId: this.company_ID
    };
    console.log(formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateDepartment', formData)
    .then((data: any) => {
        console.log(data);
        this.closeDialog();
        swal('', 'Department Update Successfully !', 'success');
        this.onAdd.emit();
    });

  }


  updateoem(value): void {

    const formData = {
      uid: this.userID,
      name: this.oemForm.get('oname').value,
      desc: this.oemForm.get('odesc').value,
      id: this.data.user.OEM_ID,
      companyId: this.company_ID
    };
    console.log('formData =>>', formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateOEM', formData)
    .then((data: any) => {
        console.log(data);
        this.closeDialog();
        swal('', 'OEM Update Successfully !', 'success');
        this.onAdd.emit();
    });

  }


  updateExperties(value): void {

    const formData = {
      uid: this.userID,
      name: this.expertiesForm.get('exname').value,
      desc: this.expertiesForm.get('exdesc').value,
      id: this.data.user.COMPETENCE_Id,
      companyId: this.company_ID
    };
    console.log('formData =>>',formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateExperties', formData)
    .then((data: any) => {
        console.log(data);
        this.closeDialog();
        swal('', 'Experties Update Successfully !', 'success');
        this.onAdd.emit();
    });

  }

  updateProduct(value): void {
    if(value.productname=='' || value.productdesc=='' || value.oem_id=='' || this.userID==''){
      swal('', 'Please Insert the Product Details !', 'error');
      this.closeDialog();
      return;
    } else {
    const formData = {
      name: this.productForm.get('productname').value,
      desc: this.productForm.get('productdesc').value,
      oem_id: this.productForm.get('oem_id').value,
      uid: this.userID,
      id: this.data.user.PRODUCT_ID,
      // companyId: this.company_ID
    };

    console.log('formData =>>', formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateProduct', formData)
    .then((data: any) => {
        console.log(data);
        this.closeDialog();
        swal('', 'Product Update Successfully !', 'success');
        this.onAdd.emit();
    });
  }
  }


  updateDesign(value): void {
    const formData = {
      uid: this.userID,
      name: this.designationForm.get('designame').value,
      desc: this.designationForm.get('desigdesc').value,
      id: this.data.user.id,
      companyId: this.company_ID
    };
    console.log(formData);
    this.apiser
    .postData(environment.localurlms, 'updateDesignation', formData)
    .then((data: any) => {
        console.log(data);
        this.closeDialog();
        swal('', 'Designation Update Successfully !', 'success');
        this.onAdd.emit();
    });

  }

}
