import { User } from './../../dashboards/project-dashboard/components/user-tasks/user-tasks.component';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ApiService } from '../../service/api-service';
import { MatButtonToggleChange } from '@angular/material';
import swal from 'sweetalert';

@Component({
  selector: 'app-rolepermission',
  templateUrl: './rolepermission.component.html',
  styleUrls: ['./rolepermission.component.scss']
})
export class RolepermissionComponent implements OnInit {

  rollData: any = [];
  userID: any;
  company_ID: any;
  formData: any = [];
  roleID: any;
  displayedColumns: string[] = ['formname', 'view', 'edit', 'fullpermission'];
  constructor(private apiser: ApiService) {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    this.company_ID = JSON.parse(sessionStorage.getItem('companyId'));
  }

  ngOnInit(): void {
    this.getAllRoleData();
    // this.getAllFormDetails();
  }

  getAllRoleData(): void {
    this.apiser
      .getData(environment.localurlms, 'getRoles')
      .then((data: any) => {
        //console.log('Role data =>>>>', data.recordset);
        this.rollData = data.recordset;
        let logRoleid=sessionStorage.getItem('roleId');
          if(logRoleid!='1'){
              this.rollData = this.rollData.filter(d => d.RoleId!=1);
            //  console.log(this.rollData);
          }
      });
  }

  onRollSelection(value): void {
    this.roleID = value;
    if (this.roleID) {
      this.getAllFormDetails();
    } else { }
  }


  getAllFormDetails(): void {
     const queryData = {
      roleId: this.roleID,
      companyId: this.company_ID
     };

    this.apiser
    .getData(environment.localurlms, 'FormDetails', queryData)
    .then((data: any) => {
        this.formData = data.recordset;
      });
  }

  selectView(form, e: MatButtonToggleChange): void {
    form && e.source.checked ? form.View = 1 : form.View = 0;
  }

  selectEdit(form, e: MatButtonToggleChange): void {
    form && e.source.checked ? form.Edit = 1 : form.Edit = 0;
  }

  selectfullpermission(form, e: MatButtonToggleChange): void {
    form && e.source.checked ? form.FullPermission = 1 : form.FullPermission = 0;
    form && e.source.checked ? form.View = 1 : form.View = 0;
    form && e.source.checked ? form.Edit = 1 : form.Edit = 0;
  }


  updateRoleSatatus(value): void {
 

    if (!this.roleID) {
      swal('error', 'Please Choose The Role', 'error');
      return;
    } else if (!this.company_ID) {
      swal('error', 'Something Went Wrong Please Check', 'error');
      return;
    } else if (!this.userID) {
      swal('error', 'Something Went Wrong Please Check', 'error');
      return;
    }

    const formData = {
      RecList: value,
      roleId: this.roleID,
      companyId: this.company_ID,
      uid: this.userID
    };

    this.apiser
    .postData(environment.localurlms, 'Role_Permission', formData)
    .then((data: any) => {

      if (data.rowsAffected.length > 0) {
        swal('', 'Updated Successfully', 'success');
        this.getAllFormDetails();
      } else {
        swal('error', 'Something Went Wrong Please Check', 'error');
      }
    });

  }

}
