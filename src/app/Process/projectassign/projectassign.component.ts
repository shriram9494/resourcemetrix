import { Component, OnInit, ViewChild, ValueProvider } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatStepper, MatSort } from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import swal from 'sweetalert';
import {PageEvent} from '@angular/material';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-projectassign',
  templateUrl: './projectassign.component.html',
  styleUrls: ['./projectassign.component.scss'],
  providers: [
    DatePipe,   { provide: ApiService, useClass: ApiService}
  ],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [
        style({ opacity: '0.2' }),
        animate(555)
      ]),
      transition('* => void', [
        style({ opacity: '2' })
      ])
    ])
  ]
})
export class ProjectassignComponent implements OnInit {

  submitted: boolean = false;
  showEmptable: boolean = true;
  showAddempform: boolean = false;
  addbutton: boolean = true  ;
  cancelbutton: boolean = false;
  project = [];
  availemployee = [];
  busyemployee = [];
  formdate: any;
  todate: any;
  employeedetails = [];
  tableShow: boolean = false;
  asignId: any;
  MatTableData: any;
  showFilter: boolean = true;
  department = [];
  technologies = [];
  positions = [];
  prj_Id: any;
  prj_Assign_Id: any;
  resopnseData: any = [];
  projData: any = [];
  projId: any;
  projEmpID: number = 0;
  uempid: any;
  userID: any;
  re_Assign: any = [];
  company_ID: any;
  requirementCount: any;
  fmaxdate: any;
  tomindate: any;
  Afdate: any;
  Atdate: any;
  displayedColumns: string[] = ['org', 'porjectName', 'fdate', 'tdate', 'department', 'technology', 'competence', 'count', 'Manage'];
  // tslint:disable-next-line: typedef

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
  projectFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder, private router: Router,
     private apiser: ApiService, private activatedRoute: ActivatedRoute,
     private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.createEmployeForm();
    this.getOrg();
    this.uempid = sessionStorage.getItem('usrEmpId');
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    this.company_ID = JSON.parse(sessionStorage.getItem('companyId'));
    this.activatedRoute.params.subscribe(params => {
      console.log('Params ->>', params);
      this.fmaxdate = new Date(params.fdate);
      this.tomindate = new Date(params.tdate);

      this.prj_Id = params.prj_Id;
      this.prj_Assign_Id = params.projectAssignid;
      this.re_Assign = params;
      if (this.re_Assign.prj_Id > 0) {
        this.projId = this.prj_Id;
        this.getProjectUpdate();
        this.showAddempform = true;
        this.showEmptable = false;
        const obj = this.re_Assign;
        this.projectFormGroup.controls['Organization'].setValue(obj.org);
        this.projectFormGroup.controls['projectName'].setValue(obj.porjectName);
        this.projectFormGroup.controls['fdate'].setValue(obj.fdate);
        this.projectFormGroup.controls['tdate'].setValue(obj.tdate);
        this.projectFormGroup.controls['department'].setValue(obj.department);
        this.projectFormGroup.controls['technology'].setValue(obj.technology);
        this.projectFormGroup.controls['positions'].setValue(obj.competence);
        this.projectFormGroup.controls['OrgId'].setValue(obj.orgId);
        this.projectFormGroup.controls['prjId'].setValue(obj.prj_Id);
        this.projectFormGroup.controls['techId'].setValue(obj.tech);
        this.showFilter = false;
      } else {
        this.showAddempform = false;
        this.showEmptable = true;
      }

    });

  }


  getProjectUpdate(): void {
    const objj = {
      prj_Id: this.prj_Id,
      companyId: sessionStorage.getItem('companyId')
    };

      this.apiser.getData(environment.localurlms, 'getProjectAssignEmployeeDetails', objj).then((t: any) => {
        const obj: any = {};
        // console.log('getProjectAssignEmployeeDetails =>',  t.recordset);
        // console.log('t.recordset-----', t.recordset[0]);
        // console.log('t.recordset-----', t.recordset[0].aEmployee);
       this.projEmpID = t.recordset[0].PRJA_EmpId;
        obj.aEmployee = t.recordset[0].aEmployee;
        obj.fdate = t.recordset[0].fdate;
        obj.tdate = t.recordset[0].tdate;

        this.employeedetails.push(obj);

        console.log(this.employeedetails);

        console.log('this.employeedetails', this.employeedetails[0]);
        if (this.employeedetails.length > 0) {
            this.tableShow = true;
        } else {
            this.tableShow = false;
        }
      });
    }

  getOrg(): void {
    const obj: any = {};
    obj.empid = sessionStorage.getItem('usrEmpId');
    obj.companyId = sessionStorage.getItem('companyId');
    console.log(obj);
    this.apiser.getData(environment.localurlms, 'getunAssignProject', obj).then((t: any) => {
              this.project = t.recordset;
              console.log('COMP---- =>', this.project);
              this.MatTableData = new MatTableDataSource();
              this.MatTableData.data = t.recordset;
              this.MatTableData.paginator = this.paginator;
              this.MatTableData.sort = this.sort;
     });
  }

  createEmployeForm(): void {
    this.projectFormGroup = this._formBuilder.group({
     'mobile': [null, Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
     'dob': [null],
      'email': [null, Validators.required],
      'Organization': [null, Validators.required],
      'projectName': [null, Validators.required],
      'technology': [null, Validators.required],
      'department': [null, Validators.required],
      'fdate': [null, Validators.required],
      'tdate': [null, Validators.required],
      'Afdate': [null, Validators.required],
      'Atdate': [null, Validators.required],
      'positions': [null, Validators.required],
      'address': [null],
      'aEmployee': [null, Validators.required],
      'bEmployee': [null],
      'OrgId': [null],
      'prjId': [null],
      'techId': [null]
    });
  }

  cancleEmp() {
      this.cancelbutton = false;
      this.showEmptable = true;
  }


  // submit(data, stepper: MatStepper): void {
  //   if(this.employeedetails.length>0){
  //     let uid=sessionStorage.getItem('usrID');
  //     let companyId=sessionStorage.getItem('companyId');
  //     data.companyId=companyId;
  //     data.uid=uid;
  //     data.emptabl=this.employeedetails;
  //    // console.log(data);
  //     this.apiser.postData(environment.localurlms,'AllocatePorject',data).then((t:any)=> {
  //           if(t.rowsAffected[0]==1){
  //             swal('','Data Inserted Successfully','success');
  //             this.projectFormGroup.reset();
  //             this.employeedetails=[];
  //             this.submitted=false;
  //           }else{
  //             swal('','Error While Insert','error');
  //           }
  //      }).catch(e => {
  //       console.log(e);
  //       // this.error = e;
  //     });
  //   }else{
  //   swal('Please Insert Employee');
  //   }
  // }

  removeparticular(i: any): void {
    this.employeedetails.splice(i, 1);
    if (this.employeedetails.length === 0 ) {
      this.tableShow = false;
    } else {
      this.tableShow = true;
    }
  }


  applyFilter(filterValue: string): void {
    this.MatTableData.filter = filterValue.trim().toLowerCase();
    }


  getFromdate(fdt) {
    this.formdate = this.datePipe.transform(fdt, 'dd MMM yyyy');
    console.log(this.formdate);
  }

  getTodate(todt) {
    this.todate = this.datePipe.transform(todt, 'dd MMM yyyy');
    console.log('To Date =>', this.todate);
  }


    assign(data: any): void {
      const obj = data;
      console.log('OBJ ->>>>>>>>', obj);
      this.asignId = obj.status1;
      this.showAddempform = true;
      this.cancelbutton = true;
      this.showEmptable = false;
      this.showFilter = false;
      this.employeedetails = [];
      this.requirementCount = obj.PRJD_COUNT;
      this.projectFormGroup.controls['Organization'].setValue(obj.org);
      this.projectFormGroup.controls['projectName'].setValue(obj.porjectName);
      this.projectFormGroup.controls['fdate'].setValue(obj.fdate);
      this.projectFormGroup.controls['tdate'].setValue(obj.tdate);
      this.projectFormGroup.controls['department'].setValue(obj.department);
      this.projectFormGroup.controls['technology'].setValue(obj.technology);
      this.projectFormGroup.controls['positions'].setValue(obj.competence);
      this.projectFormGroup.controls['OrgId'].setValue(obj.orgId);
      this.projectFormGroup.controls['prjId'].setValue(obj.prj_Id);
      this.projectFormGroup.controls['techId'].setValue(obj.tech);
      this.fmaxdate = new Date(obj.fdate);
      this.tomindate = new Date(obj.tdate);

      obj.empid = this.uempid;
      obj.companyId = sessionStorage.getItem('companyId');
      this.apiser.getData(environment.localurlms, 'getTechemp', obj).then((t: any) => {
         this.availemployee = t.recordsets[0];
         this.busyemployee = t.recordsets[1];
      });
    }

    adddata(): void {
      console.log(this.projectFormGroup.value.Afdate);
  // tslint:disable-next-line: max-line-length
      if (this.projectFormGroup.value.aEmployee == null &&
                  this.projectFormGroup.value.bEmployee == null ||
                       (this.projectFormGroup.value.Afdate == null ||
                            this.projectFormGroup.value.Atdate == null ||
                                 this.projectFormGroup.value.Afdate === '' ||
                                    this.projectFormGroup.value.Atdate === '' )) {
          swal('', 'Please select Empoyee or date', 'error');
        } else {
          this.tableShow = true;
          const obj: any = {};
          obj.aEmployee = this.projectFormGroup.value.aEmployee;
          obj.fdate = this.projectFormGroup.value.Afdate;
          obj.tdate = this.projectFormGroup.value.Atdate;

            this.employeedetails.push(obj);
            // console.log('count ->>>', this.employeedetails.length);
            if (this.employeedetails.length > this.requirementCount) {
              swal('', 'You Can Add Only' + '    ' + this.requirementCount + '     ' + 'Employee', 'warning');
              this.employeedetails.splice(-1, 1);
            } else {

            }
        }
    }


    submit(data: any): void {
            if (this.employeedetails.length > 0) {
              const uid = sessionStorage.getItem('usrID');
              const companyId = sessionStorage.getItem('companyId');
              data.companyId = companyId;
              data.uid = uid;
              data.emptabl = this.employeedetails;
              this.apiser.postData(environment.localurlms, 'AllocatePorject', data).then((t: any) => {
                    if (t.rowsAffected[0] === 1) {
                      swal('', 'Data Inserted Successfully', 'success');
                      this.projectFormGroup.reset();
                      this.employeedetails = [];
                      this.submitted = false;
                      this.showAddempform = false;
                      this.showEmptable = true;
                      this.getOrg();
                    } else {
                      swal('', 'Error While Insert', 'error');
                    }
              }).catch(e => {
                console.log(e);
                // this.error = e;
              });
            } else {
            swal('Please Insert Employee');
            this.tableShow = false;
            }
      }

      ReAssignedProject(data: any): void {
        console.log('data =>>>>>>', data);
        data[0].EmpId = this.projEmpID;
        // data[0].id =  this.prj_Id;
            if (data.length > 0) {

              const formData: any = {};
              formData.emptabl = data;
              formData.prjId = this.prj_Id;
              formData.OrgId = this.re_Assign.orgId;
              formData.techId = this.re_Assign.Tech_id;
              formData.positions = this.re_Assign.competence;
              formData.uid = this.userID;
              formData.companyId = this.company_ID;
              formData.id = this.prj_Id;

              console.log('formData ->>>>>>>>', formData);

              this.apiser.postData(environment.localurlms, 'ReAssignProjectEmployee', formData).then((t: any) => {
                console.log('t ->>>>>>>>', t);

                    if (t.rowsAffected[0] === 1) {
                      swal('', 'Project Update Successfully', 'success');
                      this.projectFormGroup.reset();
                      this.employeedetails = [];
                      this.submitted = false;
                      this.router.navigateByUrl('/dashboards/assignedproject');
                    } else {
                      swal('', 'Error While Insert', 'error');
                    }
              }).catch(e => {
                console.log(e);
              });
            } else {
            swal('Please Insert Employee');
            }
      }

}
