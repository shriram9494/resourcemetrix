import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedprojectComponent } from './assignedproject.component';

describe('AssignedprojectComponent', () => {
  let component: AssignedprojectComponent;
  let fixture: ComponentFixture<AssignedprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignedprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
