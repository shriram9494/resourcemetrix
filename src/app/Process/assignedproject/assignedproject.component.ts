import { Component, OnInit, ViewChild, ValueProvider } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatStepper, MatSort } from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import swal from 'sweetalert';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-assignedproject',
  templateUrl: './assignedproject.component.html',
  styleUrls: ['./assignedproject.component.scss'],
  providers: [
    DatePipe,   { provide: ApiService, useClass: ApiService}
  ],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [
        style({ opacity: '0.2' }),
        animate(555)
      ]),
      transition('* => void', [
        style({ opacity: '2' })
      ])
    ])
  ]
})
export class AssignedprojectComponent implements OnInit {



  submitted:boolean=false;
  showEmptable: boolean = true;
  showAddempform: boolean = false;
  addbutton: boolean = true  ;
  cancelbutton: boolean = false;
  project = [];
  availemployee = [];
  busyemployee = [];
  formdate: any;
  todate: any;
  employeedetails = [];
  tableShow: boolean = false;
  asignId: any;
  MatTableData: any;

  department = [];
  technologies = [];
  positions = [];

  displayedColumns: string[] = ['org', 'porjectName', 'fdate', 'tdate', 'department', 'technology', 'competence','count','Manage'];
  // tslint:disable-next-line: typedef

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
  projectFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder, private apiser: ApiService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.createEmployeForm();
    this.getOrg();
  }

  getOrg(): void {
    const obj: any = {};
    obj.empid = sessionStorage.getItem('usrEmpId');
    obj.companyId = sessionStorage.getItem('companyId');
    console.log(obj);
    this.apiser.getData(environment.localurlms, 'getReAssignProject', obj).then((t: any) => {
              this.project = t.recordset;
              console.log('status Id =>', this.project);
              this.MatTableData = new MatTableDataSource();
              this.MatTableData.data = t.recordset;
              this.MatTableData.paginator = this.paginator;
              this.MatTableData.sort = this.sort;
     });
  }


  // getOrg(): void {
  //   const obj: any = {};
  //   obj.empid = sessionStorage.getItem('usrEmpId');
  //   obj.companyId = sessionStorage.getItem('companyId');
  //   console.log(obj);
  //   this.apiser.getData(environment.localurlms, 'getunAssignProject', obj).then((t: any) => {
  //             this.project = t.recordset;
  //             console.log('status Id =>', this.project);
  //             this.MatTableData = new MatTableDataSource();
  //             this.MatTableData.data = t.recordset;
  //             this.MatTableData.paginator = this.paginator;
  //             this.MatTableData.sort = this.sort;
  //    });
  // }

  createEmployeForm(): void {
    this.projectFormGroup = this._formBuilder.group({
     'mobile': [null, Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
     'dob': [null],
      'email': [null, Validators.required],
      'Organization': [null, Validators.required],
      'projectName': [null, Validators.required],
      'technology': [null, Validators.required],
      'department': [null, Validators.required],
      'fdate': [null, Validators.required],
      'tdate': [null, Validators.required],
      'Afdate': [null, Validators.required],
      'Atdate': [null, Validators.required],
      'positions': [null, Validators.required],
      'address': [null],
      'aEmployee': [null, Validators.required],
      'bEmployee': [null],
      'OrgId': [null],
      'prjId': [null],
      'techId': [null]
    });
  }

  cancleEmp(){
      this.cancelbutton = false;
      this.showEmptable = true;
  }

  adddata(){
    console.log(this.projectFormGroup.value.Afdate);
    if(this.projectFormGroup.value.aEmployee==null && this.projectFormGroup.value.bEmployee==null || (this.projectFormGroup.value.Afdate==null || this.projectFormGroup.value.Atdate==null || this.projectFormGroup.value.Afdate=="" || this.projectFormGroup.value.Atdate=="" )  ){
        swal('','Please select Empoyee or date','error');
      }else{
        this.tableShow=true;
        let obj:any={};
        obj.aEmployee=this.projectFormGroup.value.aEmployee;
        obj.fdate=this.projectFormGroup.value.Afdate;
        obj.tdate=this.projectFormGroup.value.Atdate;
        this.employeedetails.push(obj);
        console.log(this.employeedetails);
      }
  }


  removeparticular(i): void {
    this.employeedetails.splice(i, 1);
  }


  applyFilter(filterValue: string): void {
    this.MatTableData.filter = filterValue.trim().toLowerCase();
    }


  getFromdate(fdt): void {
    this.formdate = this.datePipe.transform(fdt, 'dd MMM yyyy');
    console.log(this.formdate);
  }

  getTodate(todt): void {
    this.todate = this.datePipe.transform(todt, 'dd MMM yyyy');
    console.log('To Date =>', this.todate);
  }


    assign(obj: any): void {
      alert('assign');
      console.log('OBJ ->>>>>>>>', obj.status1);
      this.asignId = obj.status1;
      this.showAddempform = true;
      this.cancelbutton = true;
      this.showEmptable = false;
      this.projectFormGroup.controls['Organization'].setValue(obj.org);
      this.projectFormGroup.controls['projectName'].setValue(obj.porjectName);
      this.projectFormGroup.controls['fdate'].setValue(obj.fdate);
      this.projectFormGroup.controls['tdate'].setValue(obj.tdate);
      this.projectFormGroup.controls['department'].setValue(obj.department);
      this.projectFormGroup.controls['technology'].setValue(obj.technology);
      this.projectFormGroup.controls['positions'].setValue(obj.competence);
      this.projectFormGroup.controls['OrgId'].setValue(obj.orgId);
      this.projectFormGroup.controls['prjId'].setValue(obj.prj_Id);
      this.projectFormGroup.controls['techId'].setValue(obj.tech);
      const uempid = sessionStorage.getItem('usrEmpId');
      obj.empid = uempid;
      obj.companyId = sessionStorage.getItem('companyId');
      this.apiser.getData(environment.localurlms, 'getTechemp', obj).then((t: any) => {
         this.availemployee = t.recordsets[0];
         this.busyemployee = t.recordsets[1];
      });
    }


    submit(data): void {
      if (this.asignId == 0) {
            if (this.employeedetails.length > 0) {
              const uid = sessionStorage.getItem('usrID');
              const companyId = sessionStorage.getItem('companyId');
              data.companyId = companyId;
              data.uid = uid;
              data.emptabl = this.employeedetails;
              this.apiser.postData(environment.localurlms, 'AllocatePorject', data).then((t:any)=> {
                    if (t.rowsAffected[0] == 1) {
                      swal('', 'Data Inserted Successfully', 'success');
                      this.projectFormGroup.reset();
                      this.employeedetails = [];
                      this.submitted = false;
                    } else {
                      swal('', 'Error While Insert', 'error');
                    }
              }).catch(e => {
                console.log(e);
                // this.error = e;
              });
            } else {
            swal('Please Insert Employee');
            }
      } else {
        console.log('data =>>', data);
        //write the reassign code here
      }
    }


}
