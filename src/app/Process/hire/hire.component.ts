import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-hire',
  templateUrl: './hire.component.html',
  styleUrls: ['./hire.component.scss'],
  providers: [DatePipe, { provide: ApiService, useClass: ApiService }],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class HireComponent implements OnInit {

  displayedColumns: string[] = ['department', 'Technology', 'level', 'noofemp', 'fromdate', 'action'];
  show: boolean = false;
  showTable: boolean = true;
  cardName: string = 'New Hire';
  add: boolean = true;
  remove: boolean = false;
  techData: any = [];
  hireForm: FormGroup;
  submit: boolean = true;
  update: boolean = false;
  oem_ID: any;
  userID: any;
  oemData: any = [];
  technology_ID: any;
  data: any;
  MyTableDataSource: any;
  departments: any;
  department_ID: any;
  compitanceData: any = [];
  experties_ID: any;
  hireData: any = [];
  company_ID: any;
  formdate: any;
  fromdate = new Date();
  updateDate: any;
  addDate: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService, private datePipe: DatePipe,
     private fb: FormBuilder) {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    this.company_ID = JSON.parse(sessionStorage.getItem('companyId'));

    console.log('sesstion id =>>>>', this.userID);
   }


  ngOnInit(): void {

    this.getAllTechnologyDetails();
    this.createHireForm();
    this.getTechdep();
    this.getAllExpertiesDetails();
  }

  getTechdep(): void {
    this.apiser.getData(environment.localurlms, 'techdepdetaisl').then((t: any) => {

        this.departments = t.recordsets[1];
        this.hireData = t.recordsets[4];
        console.log('sasa', this.hireData);
        this.MyTableDataSource = new MatTableDataSource();
        this.MyTableDataSource.data = this.hireData;
        this.MyTableDataSource.sort = this.sort;
        this.MyTableDataSource.paginator = this.paginator;

    });
  }

  createHireForm(): void {
    this.hireForm = this.fb.group({
      department: ['', Validators.required],
      technology: ['', Validators.required],
      experties: ['', Validators.required],
      noofemploy: ['', Validators.required],
      // fromdate: ['', Validators.required]
    });

  }

  // getFromdate(fdt) {
  //   this.formdate = this.datePipe.transform(fdt, 'dd MMM yyyy');
  //   console.log(this.formdate);
  // }

  getAllExpertiesDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getExperties')
      .then((data: any) => {
        console.log('Competance =>>>>', data.recordset);
        this.compitanceData = data.recordset;
      });
  }

  getAllTechnologyDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getTechnology')
      .then((data: any) => {
        console.log('Technology data =>>>>', data.recordset);
        this.techData =  data.recordset;
      });
  }

  onDepartmentSelection(value): void {
    this.department_ID = value;
  }

  onTechnologySelection(value): void {
    this.technology_ID = value;
  }

  onExpertiesSelection(value): void {
    console.log(value);
    this.experties_ID = value;

   }

  addProductpenDialog(): void {
    this.show = true;
    this.showTable = false;
    this.remove = true;
    this.add = false;
    this.submit = true;
    this.update = false;
    this.fromdate = null;
    this.createHireForm();
  }

  cancelTechnologyopenDialog(): void {
    this.show = false;
    this.showTable = true;
    this.remove = false;
    this.add = true;
    this.submit = true;
    this.update = false;
    // this.fromdate = '';
    this.fromdate = null;
    this.createHireForm();
    this.getTechdep();
  }

  editHireData(hire): void {
    this.data = hire;
    this.cardName = 'Update Hire';
    this.show = true;
    this.showTable = false;
    this.remove = true;
    this.add = false;
    this.submit = false;
    this.update = true;

    this.hireForm = this.fb.group({
      experties: [this.data.COMPETENCE_Id],
      department: [this.data.DEPT_ID],
      technology: [this.data.TECH_ID],
      noofemploy: [this.data.HIRE_NOOFEMP],
      // fromdate: [this.data.HIRE_UPTODATE]
    });
    this.fromdate = this.data.HIRE_UPTODATE;
  }


  addHireForm(value): void {
      this.addDate = new Date(this.fromdate);
      this.addDate.setMinutes( this.addDate.getMinutes() + 480 );

         const formData = {
        dname: this.department_ID,
        noofemp: this.hireForm.get('noofemploy').value,
        experties_id: this.experties_ID,
        technology_id: this.technology_ID,
        fdate: this.addDate,
        company: this.company_ID,
        uid: this.userID
      };

      this.apiser
      .postData(environment.localurlms, 'Hireinsert', formData)
      .then((data: any) => {
        if (data.rowsAffected.length > 0) {
          swal('', 'Hire Request Added Successfully !', 'success');
          this.add = true;
          this.remove = false;
          this.showTable = true;
          this.show = false;
          this.getTechdep();
          this.createHireForm();

        } else {
          swal('', 'Somethind Went Wrong Please Check !', 'error');

        }
      });
    }


    updateHireForm(value): void {

      this.updateDate = new Date(this.fromdate);
      this.updateDate.setMinutes( this.updateDate.getMinutes() + 480 );

      if (value.noofemploy == null || this.fromdate == null ) {
        swal('', 'All Fields Are Mandatory', 'error');
          return;
      } else {
        const formData = {
          dname: this.hireForm.get('department').value,
          noofemp: this.hireForm.get('noofemploy').value,
          experties_id: this.hireForm.get('experties').value,
          technology_id: this.hireForm.get('technology').value,
          fdate: this.updateDate,
          company: this.company_ID,
          uid: this.userID,
          id: this.data.HIRE_ID
        };
        console.log('formData =>>>>>>>>>>>>>', formData);

        this.apiser
        .postData(environment.localurlms, 'HireUpdate', formData)
        .then((data: any) => {
          this.createHireForm();
          this.add = true;
          this.show = false;
          this.showTable = true;
          this.getTechdep();
          this.remove = false;
          this.update = false;
          this.submit = false;
          // this.fromdate = '';
          swal('', 'Hire Update Successfully !', 'success');
        });
      }
  }


  confirmOpenDialog(): void {
    const label = 'Are you sure confir delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
    }

  }
