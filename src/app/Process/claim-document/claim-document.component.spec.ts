import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimDocumentComponent } from './claim-document.component';

describe('ClaimDocumentComponent', () => {
  let component: ClaimDocumentComponent;
  let fixture: ComponentFixture<ClaimDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
