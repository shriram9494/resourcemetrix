import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';



@Component({
  selector: 'app-claim-document',
  templateUrl: './claim-document.component.html',
  styleUrls: ['./claim-document.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class ClaimDocumentComponent implements OnInit {


  displayedColumns: string[] = ['id', 'poem', 'tname', 'pname', 'description', 'action','status'];
  show: boolean = false;
  showTable: boolean = true;
  cardName: string = 'Add New';
  add: boolean = true;
  remove: boolean = false;
  techData: any = [];
  claimForm: FormGroup;
  submit: boolean = true;
  update: boolean = false;
  oem_ID: any;
  userID: any;
  oemData: any = [];
  technology_ID: any;
  data: any;
  MyTableDataSource: any;
  company_ID: any;
  filesToUpload: Array<File> = [];
  claimFile: any;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService, private fb: FormBuilder) {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    console.log('sesstion id =>>>>', this.userID);
    this.company_ID = sessionStorage.getItem('companyId');

   }


  ngOnInit(): void {

  }


  createClaimForm(): void {
    this.claimForm = this.fb.group({
      project_name: ['', Validators.required],
      claimDate: ['', Validators.required],
      claimDescription: ['', Validators.required]
    });

  }


  addProductpenDialog(): void {

    this.show = true;
    this.showTable = false;
    this.remove = true;
    this.add = false;
    this.submit = true;
    this.update = false;
    this.createClaimForm();
  }


  cancelTechnologyopenDialog(): void {
    this.show = false;
    this.showTable = true;
    this.remove = false;
    this.add = true;
    this.createClaimForm();
  }


  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    //this.product.photo = fileInput.target.files[0]['name'];
    console.log(this.filesToUpload);
}


  uploadFile(): void {
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    console.log(files);

    for (let i = 0; i < files.length; i++) {
        formData.append("uploads[]", files[i], files[i]['name']);
    }
    console.log('form data variable : ' + formData.toString());


}

  // statuschange(usr: any): void {
  //   const data: any = {};
  //   data.id = usr.PRODUCT_ID;
  //   data.companyId = sessionStorage.getItem('companyId');
  //   data.status = usr.IsActive;
  //   console.log(data);
  //   this.apiser.postData(environment.localurlms, 'updteProductstatus', data).then((t: any) => {
  //     // tslint:disable-next-line: triple-equals
  //     if (t.rowsAffected[0] == 1) {
  //       swal('', 'Status update sucessfull', 'success');
  //       this.getAllProductDetails();
  //       } else {
  //       swal('', 'Error While update', 'error');
  //     }
  //   });
  // }


  }
