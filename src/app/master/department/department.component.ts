import { ApiService } from './../../service/api-service';
import { User } from './../../dashboards/project-dashboard/components/user-tasks/user-tasks.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { MatTableDataSource, MatPaginator, MatDialog, MatSort } from '@angular/material';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import { Department } from '../../shared/model/department';
import swal from 'sweetalert';


@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})

export class DepartmentComponent implements OnInit {


  MyDataSource: any;
  queryData: Department = new Department();

  displayedColumns: string[] = ['id', 'dname', 'description', 'action','status'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService) {}

  ngOnInit(): void {
    this.getAllDepartmentDetails();

  }


  getAllDepartmentDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getDepartment')
      .then((data: any) => {
        console.log('deopartment data =>>>>', data.recordset);
        this.MyDataSource = new MatTableDataSource();
        this.MyDataSource.data = data.recordset;
        this.MyDataSource.paginator = this.paginator;
        this.MyDataSource.sort = this.sort;
      });
  }

  addDepartmentopenDialog(): void {
    const dialogName = 'Add New Department';
    const dialogId = 1;
    const diaglogRef = this.dialog.open(AdddepartmentComponent, {
      data: {
        dialogName,
        dialogId
      }
    });

    diaglogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllDepartmentDetails();
    });

  }
  statuschange(usr):void{
    let data:any={};
    data.id=usr.DEPT_ID;
    data.companyId=sessionStorage.getItem('companyId');
    data.status=usr.DEPT_Status;
    console.log(data);
    this.apiser.postData(environment.localurlms, 'updateDepartmenttatus', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfully !', 'success');
        this.getAllDepartmentDetails();
        } else {
        swal('', 'Error While update', 'error');
      }
    })
  }


  editDepartmentopenDialog(user): void {
    const dialogName = 'Update Department';
    const dialogId = 1;
    const DEPT_ID = 11;
    const dialogRef = this.dialog.open(AdddepartmentComponent, {
      data: { user,
        dialogName,
        dialogId,
        DEPT_ID
      }

    });

    dialogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllDepartmentDetails();
    });
  }

  confirmOpenDialog(): void {
    const label = 'Are you sure confir delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
  }



}

