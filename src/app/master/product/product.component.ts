import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class ProductComponent implements OnInit {

  displayedColumns: string[] = ['id', 'poem', 'tname', 'pname', 'description', 'action','status'];
  show: boolean = false;
  showTable: boolean = true;
  cardName: string = 'Add New Product';
  add: boolean = true;
  remove: boolean = false;
  techData: any = [];
  productForm: FormGroup;
  submit: boolean = true;
  update: boolean = false;
  oem_ID: any;
  userID: any;
  oemData: any = [];
  technology_ID: any;
  data: any;
  MyTableDataSource: any;
  company_ID: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService, private fb: FormBuilder) {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    console.log('sesstion id =>>>>', this.userID);
    this.company_ID = sessionStorage.getItem('companyId');

   }


  ngOnInit() {
    this.getAllProductDetails();
    this.getAllTechnologyDetails();
    this.createProductForm();
    this.getAllOEMDetails();
  }


  createProductForm(): void {
    this.productForm = this.fb.group({
      productname: ['', Validators.required],
      oem_id: ['', Validators.required],
      tname: ['', Validators.required],
      productdesc: ['', Validators.required]
    });

  }



  getAllProductDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getProduct')
      .then((data: any) => {
        console.log('PRODUCT =>>>>', data.recordset);
        this.MyTableDataSource = new MatTableDataSource();
        this.MyTableDataSource.data = data.recordset;
        this.MyTableDataSource.sort = this.sort;
        this.MyTableDataSource.paginator = this.paginator;
      });
  }


  getAllTechnologyDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getTechnology')
      .then((data: any) => {
        console.log('Technology data =>>>>', data.recordset);
        this.techData =  data.recordset;
      });
  }


  getAllOEMDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getOEM')
      .then((data: any) => {
        console.log('getOEM data =>>>>', data.recordset);
        this.oemData = data.recordset;
      });
  }

  onOemSelection(value): void {
    this.oem_ID = value;
    console.log(this.oem_ID);

  }

  onTechnologySelection(value): void {
    this.technology_ID = value;
  }

  addProductpenDialog(): void {

    this.show = true;
    this.showTable = false;
    this.remove = true;
    this.add = false;
    this.submit = true;
    this.update = false;
    this.createProductForm();
  }


  cancelTechnologyopenDialog(): void {
    this.show = false;
    this.showTable = true;
    this.remove = false;
    this.add = true;
    this.getAllProductDetails();
    this.createProductForm();
  }


  editProductopenDialog(user): void {
    this.data = user;
    console.log('Data = >>>---->', this.data);

    this.cardName = 'Update Product';
    this.show = true;
    this.showTable = false;
    this.remove = true;
    this.add = false;
    this.submit = false;
    this.update = true;
    this.productForm.controls['oem_id'].setValue(this.data.OEM_ID, { onlySelf: false });
    this.productForm.controls['tname'].setValue(this.data.Technology_Id, { onlySelf: false });
    this.productForm.controls['productdesc'].setValue(this.data.PRODUCT_Description, { onlySelf: true });
    this.productForm.controls['productname'].setValue(this.data.PRODUCT_Name, { onlySelf: true });

  }

  statuschange(usr: any): void {
    const data: any = {};
    data.id = usr.PRODUCT_ID;
    data.companyId = sessionStorage.getItem('companyId');
    data.status = usr.IsActive;
    console.log(data);
    this.apiser.postData(environment.localurlms, 'updteProductstatus', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfull', 'success');
        this.getAllProductDetails();
        } else {
        swal('', 'Error While update', 'error');
      }
    });
  }

  addprouctForm(value): void {
    if (value.productname =='' || value.productdesc =='' || value.oemid =='' || this.userID =='') {
      swal('', 'Please Insert the Product Details !', 'error');
      return;
    }

      const formData = {

        name: this.productForm.get('productname').value,
        desc: this.productForm.get('productdesc').value,
        oem_id: this.oem_ID,
        technology_id: this.technology_ID,
        uid: this.userID,
        companyId: this.company_ID
         // oem_id: this.productForm.get('oemid').value,

      };
      console.log('FormData =>>', formData);

      this.apiser
      .postData(environment.localurlms, 'InsertProduct', formData)
      .then((data: any) => {
        console.log('Product added =>>', data.recordset);

        if (data.rowsAffected.length > 0) {
          swal('', 'Product Added Successfully !', 'success');
          this.getAllProductDetails();
          this.add = true;
          this.remove = false;
          this.showTable = true;
          this.show = false;
          this.createProductForm();


        } else {
          swal('error');

        }
      });
    }



    updateproductForm(value): void {

      console.log('data =>>>', this.data);

    this.cardName = 'Update Product';
    this.show = true;
    this.showTable = false;
    this.remove = true;
    this.add = false;

    const formData = {
      name: this.productForm.get('productname').value,
      desc: this.productForm.get('productdesc').value,
      oem_id: this.productForm.get('oem_id').value,
      uid: this.userID,
      tech_id: this.productForm.get('tname').value,
      id: this.data.PRODUCT_ID,
      companyId: this.company_ID

    };

    console.log('formData =>>', formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateProduct', formData)
    .then((data: any) => {
      this.createProductForm();
      this.add = true;
      this.show = false;
      this.showTable = true;
      this.getAllProductDetails();
      swal('', 'Product Update Successfully !', 'success');
      this.remove = false;
      this.update = false;
      this.submit = false;
    });

  }


  confirmOpenDialog(): void {
    const label = 'Are you sure confir delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
    }

  }
