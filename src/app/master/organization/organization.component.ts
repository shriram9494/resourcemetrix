import { Component, OnInit, ApplicationInitStatus, ViewChild } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatStepper,
  MatSort
} from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import swal from 'sweetalert';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
  providers: [{ provide: ApiService, useClass: ApiService }],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class OrganizationComponent implements OnInit {
  submitted: boolean = false;
  showEmptable: boolean = true;
  showAddempform: boolean = false;
  addbutton: boolean = true;
  cancelbutton: boolean = false;
  update: boolean = false;
  submitBtn: boolean = true;
  customer: any = [];
  user_UID: any;
  organizationFormGroup: FormGroup;
  addButton: boolean = true;
  MyTableData: any;
  company_ID: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private _formBuilder: FormBuilder,
    private ser: ApiService,
    private apiser: ApiService
  ) { }

  ngOnInit(): void {
    this.createEmployeForm();
    this.getOrg();
    this.user_UID = sessionStorage.getItem('usrID');
    this.company_ID = JSON.parse(sessionStorage.getItem('companyId'));

  }

  createEmployeForm(): void {
    this.organizationFormGroup = this._formBuilder.group({
      'mobile': [
        null,
        Validators.compose([Validators.minLength(10), Validators.maxLength(10)])
      ],
      'email': ['', Validators.required],
      'organizationName': ['', Validators.required],
      'contactPerson': ['', Validators.required],
      'website': ['', Validators.required],
      'address': [''],
      'fax': [''],
      'gst': [''],
      'pan': [''],
      'format_textdirection_r_to_l': ['']

    });
  }

  getOrg(): void {
    this.apiser
      .getData(environment.localurlms, 'Organiztion')
      .then((t: any) => {
        console.log('t', t);

        this.MyTableData = new MatTableDataSource();
        this.customer = t.recordset;
        this.MyTableData.data = t.recordset;
        this.MyTableData.paginator = this.paginator;
        this.MyTableData.sort = this.sort;

      });
  }

  applyFilter(filterValue: string): void {
    this.MyTableData.filter = filterValue.trim().toLowerCase();
    }


// tslint:disable-next-line: member-ordering
  displayedColumns: string[] = [
    'OrgName',
    'web',
    'contactperson',
    'contact',
    'GST',
    'Pan',
    'address1',
    'action',
    'status'
  ];
  // tslint:disable-next-line: typedef
  dataSource = new MatTableDataSource(this.customer);

  get f() {
    return this.organizationFormGroup.controls;
  }

  cusomerData: any = [];

  editCustomerdetails(obj) {
    this.cusomerData = obj;
    this.showAddempform = true;
    this.showEmptable = false;
    this.addbutton = false;
    this.cancelbutton = true;
    this.update = true;
    this.submitBtn = false;
    this.organizationFormGroup.controls['mobile'].setValue(this.cusomerData.contact);
    this.organizationFormGroup.controls['email'].setValue(this.cusomerData.email);
    this.organizationFormGroup.controls['organizationName'].setValue(
      this.cusomerData.OrgName
    );
    this.organizationFormGroup.controls['contactPerson'].setValue(
      this.cusomerData.contactperson
    );
    this.organizationFormGroup.controls['website'].setValue(this.cusomerData.web);
    this.organizationFormGroup.controls['address'].setValue(this.cusomerData.address1);
    this.organizationFormGroup.controls['gst'].setValue(this.cusomerData.GST);
    this.organizationFormGroup.controls['pan'].setValue(this.cusomerData.Pan);
    this.organizationFormGroup.controls['fax'].setValue(this.cusomerData.fax);
    // this.organizationFormGroup.controls['id'].setValue(this.cusomerData.id);
  }

  submit(data): void {
    // console.log("form data =>",data);

    if (this.organizationFormGroup.invalid) {
      this.submitted = true;
      return;
    } else {
      const uid = sessionStorage.getItem('usrID');
      data.uid = uid;
      data.companyId = this.company_ID;
      console.log("DATA ->>>", data);

      this.ser
        .postData(environment.localurlms, 'OrganizationInsert', data)
        .then((t: any) => {
          if (t.rowsAffected[0] == 1) {
            swal('', 'Customer Inserted Successfully', 'success');
            this.organizationFormGroup.reset();
            this.getOrg();
            this.submitted = false;
            this.showAddempform = false;
            this.showEmptable = true;
            this.submitBtn = true;
            this.update = false;
            this.createEmployeForm();
          } else {
            swal('', 'Error While Insert', 'error');
          }
        })
        .catch(e => {
          console.log(e);
          // this.error = e;
        });
    }
  }

  statuschange(usr: any): void {
    const data: any = {};
    data.id = usr.id;
    data.companyId = 1;
    // data.companyId=sessionStorage.getItem('companyId');
    data.status = usr.org_status;
    console.log(data);
    this.apiser.postData(environment.localurlms, 'Update_Org_Status', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfull', 'success');
        this.getOrg();
        } else {
        swal('', 'Error While update', 'error');
      }
    });
  }


  addNewemployee(): void {
    if (this.showEmptable == true) {
      this.showAddempform = true;
      this.showEmptable = false;
      this.addbutton = false;
      this.cancelbutton = true;
      this.update = false;
      this.organizationFormGroup.reset();
    } else {
      this.showAddempform = false;
      this.showEmptable = true;
      this.addbutton = true;
      this.cancelbutton = false;
      this.update = false;
      this.organizationFormGroup.reset();
    }
  }


  updateCustomer(data) {
    this.update = true;
    this.addbutton = false;
    let uid = sessionStorage.getItem('usrID');
      data.uid = uid;
      data.id = this.cusomerData.id;
      data.companyId = this.company_ID;
      console.log('formData', data);

    this.ser
      .postData(environment.localurlms, 'OrganizationUpdate', data)
      .then((t: any) => {
        if (t.rowsAffected[0] == 1) {
          swal('', 'Customer Update Successfully', 'success');
          this.organizationFormGroup.reset();
          this.getOrg();
          this.submitted = false;
          this.cancelbutton = false;
          this.addbutton = true;
          this.showAddempform = false;
          this.showEmptable = true;
          this.update = false;
          this.submitBtn = true;
          this.createEmployeForm();
        } else {
          swal('', 'Error While Insert', 'error');
        }
      })
      .catch(e => {
        console.log(e);
        // this.error = e;
      });

   }

  cancelAddemployee(): void {
    // tslint:disable-next-line: triple-equals
    this.getOrg();
    if (this.showAddempform == true) {
      this.showEmptable = true;
      this.showAddempform = false;
      this.addbutton = true;
      this.cancelbutton = false;
      this.submitted = false;
      this.submitBtn = true;
      this.update = false;
      this.createEmployeForm();
    } else {
      this.showEmptable = true;
      this.showAddempform = false;
      this.addbutton = false;
      this.cancelbutton = true;
      this.createEmployeForm();
    }
  }
}
