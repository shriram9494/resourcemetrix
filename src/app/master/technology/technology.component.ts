import { ApiService } from './../../service/api-service';
import { User } from './../../dashboards/project-dashboard/components/user-tasks/user-tasks.component';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { MatTableDataSource, MatPaginator, MatDialog, MatSort } from '@angular/material';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import { Department } from '../../shared/model/department';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import swal from 'sweetalert';



@Component({
  selector: 'app-technology',
  templateUrl: './technology.component.html',
  styleUrls: ['./technology.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class TechnologyComponent implements OnInit , AfterViewInit {
  deparData: any;
  technologyForm: FormGroup;
  show: boolean = false;
  add: boolean = true;
  submit: boolean = true;
  update: boolean = false;
  techTable: boolean = true;
  remove: boolean = false;
  userID: any;
  oemData: any;
  productData: any;
  oem_ID: any;
  user: any;
  user_Data: any;
  product_ID: any;
  cardName: string = 'Add New Technology';
// tslint:disable-next-line: typedef
  MatTableData: any;
  queryData: Department = new Department();
  company_ID: any;

  displayedColumns: string[] = ['id','dname', 'description', 'blank', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private fb: FormBuilder, private apiser: ApiService) {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));

  }

  ngOnInit(): void {
    this.getAllTechnologyDetails();
    this.createTechnologyForm();
    this.getAllOEMDetails();
    this.getAllProductDetails();
    this.company_ID = sessionStorage.getItem('companyId');

  }


  getAllOEMDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getOEM')
      .then((data: any) => {
        console.log('getOEM data =>>>>', data.recordset);
        this.oemData = data.recordset;
      });
  }


  getAllProductDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getProduct')
      .then((data: any) => {
        console.log('PRODUCT =>>>>', data.recordset);
        this.productData = data.recordset;
      });
  }


  getAllTechnologyDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getTechnology')
      .then((data: any) => {
        console.log('Technology data =>>>>', data.recordset);
        this.MatTableData = new MatTableDataSource();
        this.MatTableData.data = data.recordset;
        this.MatTableData.paginator = this.paginator;
        this.MatTableData.sort = this.sort;
      });
  }

  createTechnologyForm(): void {
    this.technologyForm = this.fb.group({
      techname: ['', Validators.required],
      techdesc: ['', Validators.required],

    });
  }

  onOemSelection(value): void {
    console.log('DropDown Value =>>>', value);
    this.oem_ID = value;
  }



  addTechnologyopenDialog(value: any): void {
    this.show = true;
    this.techTable = false;
    this.add = false;
    this.remove = true;
    this.getAllTechnologyDetails();
  }

  addtechnologyForm(value: any): void {

      if (value.techname == '' || value.techdesc ==='') {
        swal('', 'Please Submit The Form', 'error');
        return;
      }

    this.show = true;
    this.techTable = false;
    this.add = false;
    this.remove = true;

    const formData = {
      oem_Id: 0,
      name: this.technologyForm.get('techname').value,
      desc: this.technologyForm.get('techdesc').value,
      uid: this.userID,
      companyId: this.company_ID
    };

    console.log("formdata =>>", formData);

    this.apiser
    .postData(environment.localurlms, 'InsertTechnology', formData)
    .then((data: any) => {
      console.log('technology added =>>', data);
      if( data ) {
        this.getAllTechnologyDetails();
        this.show = false;
        this.techTable = true;
        this.add = true;
        this.remove = false;
        swal('', 'Technology Added Successfully !', 'success');

      } else {

      }

    });

  }


  cancelTechnologyopenDialog(): void {
    this.show = false;
    this.techTable = true;
    this.add = true;
    this.remove = false;
    this.update = false;
    this.submit = true;
    this.createTechnologyForm();
    this.getAllTechnologyDetails();
  }

  editTechnologyopenDialog(user): void {

    console.log("tch data", user);
    this.user_Data = user
    this.show = true;
    this.techTable = false;
    this.add = false;
    this.remove = true;
    this.update = true;
    this.submit = false;
    this.cardName = 'Update Technology';
    this.technologyForm.controls['techname'].setValue(this.user_Data.TECH_Name, { onlySelf: true });
    this.technologyForm.controls['techdesc'].setValue(this.user_Data.TECH_DESC, { onlySelf: true });
    // this.technologyForm.controls['oname'].setValue(this.user_Data.OEM_ID, { onlySelf: false });

  }


  updatetechnologyForm(value): void {

    let formData = {

      // oem_Id: this.oem_ID,
      oem_Id: 0,
      name: this.technologyForm.get('techname').value,
      desc: this.technologyForm.get('techdesc').value,
      uid: this.userID,
      id: this.user_Data.TECH_ID,
      companyId: this.company_ID
    };

    console.log("formdata =>>", formData);
    this.apiser
    .postData(environment.localurlms, 'UpdateTechnology', formData)
    .then((data: any) => {
        console.log(data);
        swal('', 'Technology Update Successfully !', 'success');
        this.getAllTechnologyDetails();
        this.show = false;
        this.techTable = true;
        this.add = true;
        this.remove = false;
        this.update = false;
        this.submit = false;

    });

  }


  ngAfterViewInit(): void {

  }

  confirmOpenDialog(): void {
    const label = 'Are you sure confir delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
  }


}

