import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import swal from 'sweetalert';

@Component({
  selector: 'app-experties',
  templateUrl: './experties.component.html',
  styleUrls: ['./experties.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class ExpertiesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'oname', 'description', 'action','status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  MyTableData: any;
  constructor(private dialog: MatDialog, private apiser: ApiService) { }

  ngOnInit(): void {
    this.getAllExpertiesDetails();
  }


  getAllExpertiesDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getExperties')
      .then((data: any) => {
        console.log('Experties =>>>>', data.recordset);
        this.MyTableData = new MatTableDataSource();
        this.MyTableData.data = data.recordset;
        this.MyTableData.paginator = this.paginator;
        this.MyTableData.sort = this.sort;

      });
  }

  statuschange(usr: any): void {
    const data: any = {};
    data.id=usr.COMPETENCE_Id;
    data.companyId=sessionStorage.getItem('companyId');
    data.status=usr.IsActive;
    console.log(data);
    this.apiser.postData(environment.localurlms, 'updteExpertisestatus', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfull', 'success');
        this.getAllExpertiesDetails();
        } else {
        swal('', 'Error While update', 'error');
      }
    });
  }


  addExpertiespenDialog(): void {
    const dialogName = 'Add New Experties';
    const dialogId = 2;
    const diaglogRef = this.dialog.open(AdddepartmentComponent, {
      data: {
        dialogName,
        dialogId
      }
    });

    diaglogRef.componentInstance.onAdd.subscribe((response) => {
     this.getAllExpertiesDetails();
    });
  }

  editExpertiesopenDialog(user): void {
    const dialogName = 'Update Experties';
    const dialogId = 2;
    const DEPT_ID = 22;
    const dialogRef = this.dialog.open(AdddepartmentComponent, {
      data: { user,
        dialogName,
        dialogId,
        DEPT_ID
      }

    });

    dialogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllExpertiesDetails();
    });
  }

  confirmOpenDialog(): void {
    const label = 'Are you sure confirm delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
  }

  }


