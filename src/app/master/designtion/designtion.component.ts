import { ApiService } from './../../service/api-service';
import { User } from './../../dashboards/project-dashboard/components/user-tasks/user-tasks.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { MatTableDataSource, MatPaginator, MatDialog, MatSort } from '@angular/material';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import { Department } from '../../shared/model/department';



@Component({
  selector: 'app-designtion',
  templateUrl: './designtion.component.html',
  styleUrls: ['./designtion.component.scss']
})
export class DesigntionComponent implements OnInit {
  deparData: any;

  MyTableData: any;
  queryData: Department = new Department();


  displayedColumns: string[] = ['id', 'dname', 'description', 'action','status'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService) {}

  ngOnInit(): void {
    this.getAllDedignationDetails();
  }


  getAllDedignationDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getDesignation')
      .then((data: any) => {
        console.log('Designation data =>>>>', data.recordset);
        this.MyTableData = new MatTableDataSource();
        this.MyTableData.data = data.recordset;
        this.MyTableData.paginator = this.paginator;
        this.MyTableData.sort = this.sort;
      });
  }

  addDesignationDialog(): void {
    const dialogName = 'Add New Designation';
    const dialogId = 3;

    const diaglogRef = this.dialog.open(AdddepartmentComponent, {
      data: {
        dialogName,
        dialogId
      }
    });

    diaglogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllDedignationDetails();
    });

  }

  statuschange(usr):void{
    let data:any={};
    data.id=usr.id;
    data.companyId=sessionStorage.getItem('companyId');
    data.status=usr.DES_Status;
    this.apiser.postData(environment.localurlms, 'updateDesignationstatus', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfull', 'success');
        this.getAllDedignationDetails();
        } else {
        swal('', 'Error While Insert', 'error');
      }
    })
  }


  editDesignationDialog(user): void {
    const dialogName = 'Update Designation';
    const dialogId = 3;
    const DEPT_ID = 33;
    const dialogRef = this.dialog.open(AdddepartmentComponent, {
      data: { user,
        dialogName,
        dialogId,
        DEPT_ID
      }

    });

    dialogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllDedignationDetails();
    });
  }

  confirmOpenDialog(): void {
    const label = 'Are you sure confir delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
  }

}

