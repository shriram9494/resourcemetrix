import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesigntionComponent } from './designtion.component';

describe('DesigntionComponent', () => {
  let component: DesigntionComponent;
  let fixture: ComponentFixture<DesigntionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesigntionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesigntionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
