import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatStepper,
  MatSort
} from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import swal from 'sweetalert';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [{ provide: ApiService, useClass: ApiService }],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class UsersComponent implements OnInit {
  MatTableData: any;
  usr: any = [];
  displayedColumns: string[] = ['emp', 'email', 'contact', 'status', 'Manage'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  projectFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder, private apiser: ApiService) {}

  ngOnInit() {
    this.getUser();
  }
  getUser() {
    // let uempid=sessionStorage.getItem("usrEmpId");
    this.apiser
      .getData(environment.localurlms, 'inactiveuserRet')
      .then((t: any) => {
        console.log(t.recordset);
        this.usr = t.recordset;
        this.MatTableData = new MatTableDataSource();
        this.MatTableData.data = t.recordset;
        this.MatTableData.paginator = this.paginator;
        this.MatTableData.sort = this.sort;
      });
  }
  assign(usr) {
    usr.companyId = sessionStorage.getItem('companyId');
    console.log(usr);
    this.apiser
      .getData(environment.localurlms, 'Activateusr', usr)
      .then((t: any) => {
        this.usr = t.recordset;
        this.getUser();
      });
  }


  applyFilter(filterValue: string): void {
    this.MatTableData.filter = filterValue.trim().toLowerCase();
    }

}
