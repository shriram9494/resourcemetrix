import { Position } from './../employee/employee.component';
import { Technology } from './../../shared/model/technology';
import { Component, OnInit, ViewChild, ValueProvider } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatStepper,
  MatTable,
  MatSort
} from '@angular/material';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import swal from 'sweetalert';
import { DatePipe } from '@angular/common';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-projectdetails',
  templateUrl: './projectdetails.component.html',
  styleUrls: ['./projectdetails.component.scss'],
  providers: [DatePipe, { provide: ApiService, useClass: ApiService }],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class ProjectdetailsComponent implements OnInit {
  submitted: boolean = false;
  showEmptable: boolean = true;
  showAddempform: boolean = false;
  addbutton: boolean = true;
  cancelbutton: boolean = false;
  project = [];
  porject_Data: any;
  userId: any;
  customer_ID: any;
  department_ID: any;
  technology_ID: any;
  expertise_ID: any;
  ass_tech_mgr_ID: any;
  ass_project_mgr_ID: any;
  saveID: boolean = true;
  formdate: any;
  todate: any;
  experties: any;
  technologies: any = [];
  empData: any;
  projectdetails: any = [];
  organizationList: any = [];
  tableShow: boolean = false;
  update: boolean = false;
  countryData: any;
  stateData: any;
  cityData: any;
  stateDataResult: any;
  cityDataResult: any;
  country_ID: any;
  state_ID: any;
  city_ID: any;
  emailList: any = [];
  deliverModel_ID: any;
  allConmbignData: any = [];
  deliverModelData: any = [];
  departmentModelData: any = [];
  technologyModelData: any = [];
  designationModelData: any = [];
  compitanceModelData: any = [];
  projectStatusData: any = []
  content: any;
  tableID: number = 0;
  company_ID: any;
  userID: any;
  statusShow: number = 0;
  projectStatus_ID: any;
  MyTableData: any;
  stringArr: any = [];
  tech_comp_data: any = [];
  technologyID: any;

  fdate = new Date();
  tdate = new Date();

  firsFromtDate: any;
  secondToDate: any;

  skils: any = {
    techid: '',
    comptid: '',
    count: 0
  };


  displayedColumns: string[] = [
    'org',
    'porjectName',
    'fdate',
    'tdate',
    'department',
    'proj_pm',
    'proj_tm',
    'action'
  ];
  // tslint:disable-next-line: typedef
  dataSource = new MatTableDataSource(this.project);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  projectFormGroup: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private apiser: ApiService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.createProjectForm();
    this.getProject();
    const uid = sessionStorage.getItem('usrID');
    this.userId = uid;
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    this.company_ID = JSON.parse(sessionStorage.getItem('companyId'));
    this.getAllEmpolyeDetails();
    this.getAllCountryDetails();
    this.getAllCombignData();
    this.getOrg();
  }

  getAllEmpolyeDetails() {
    this.apiser
      .getData(environment.localurlms, 'getEmployee')
      .then((data: any) => {
        this.empData = data.recordset;
        console.log('emp', this.empData);
      });
  }


  getAllCombignData(): void {
    this.apiser
      .getData(environment.localurlms, 'getDeptTechDesCompDD')
      .then((data: any) => {
        this.allConmbignData = data;
        console.log('allConmbignData =>>>>', this.allConmbignData.recordsets);
        this.departmentModelData = this.allConmbignData.recordsets[0];
        this.technologyModelData = this.allConmbignData.recordsets[1];
        this.designationModelData = this.allConmbignData.recordsets[2];
        this.compitanceModelData = this.allConmbignData.recordsets[3];
        this.deliverModelData = this.allConmbignData.recordsets[4];
        this.projectStatusData = this.allConmbignData.recordsets[6];
        console.log('projectStatusData: any = []; ->>>>', this.projectStatusData);

      });
  }


  getAllCountryDetails() {
    this.apiser
      .getData(environment.localurlms, 'Get_Contry_State_City')
      .then((data: any) => {
        this.stateDataResult = data.recordsets[0];
        this.countryData = data.recordsets[1];
        this.cityDataResult = data.recordsets[2];
      });
  }

  onCustomerSelection(value): void {
    this.customer_ID = value;
  }

  onDepartmentSelection(value): void {
    this.department_ID = value;
  }

  onTechnology2Selection(value): void {
    this.technology_ID = value;
  }

  onExpertiesSelection(value): void {
    this.expertise_ID = value;
  }

  onAsTechMgrSelection(value): void {
    this.ass_tech_mgr_ID = value;
    console.log('mgr value', this.ass_tech_mgr_ID);
  }

  onAsProjMgrSelection(value): void {
    this.ass_project_mgr_ID = value;
  }

  onCountrySelection(value): void {
    this.getAllCountryDetails();
    this.country_ID = value;
    this.stateData = this.stateDataResult.filter(
      state => state.State_Country == this.country_ID
    );
  }

  onStateSelection(value): void {
    this.state_ID = value;
    this.cityData = this.cityDataResult.filter(
      city => city.City_State_Id == this.state_ID
    );
  }

  onCitySelection(value): void {
    this.city_ID = value;
  }

  onDeliveryModelSelection(value): void {
    this.deliverModel_ID = value;
    console.log(this.deliverModel_ID);
  }


  onProjectStatusSelection(value): void {
    this.projectStatus_ID = value;
    console.log(this.projectStatus_ID);
  }

  // checkEmail(event): void {
  //   let count=this.emailList.findIndex(x => x.toLowerCase() == event.toLowerCase());
  //     if(count!=-1){
  //       this.showError = true;
  //     }
  //    else {
  //     this.showError = false;
  //   }

  // }

  getOrg() {
    this.apiser
      .getData(environment.localurlms, 'Organiztion')
      .then((t: any) => {
        this.organizationList = t.recordset;
        console.log('aaaaaa', this.organizationList);


      });
  }

  // department: any;

  // getTechdep() {
  //   this.apiser
  //     .getData(environment.localurlms, 'techdepdetaisl')
  //     .then((t: any) => {
  //       this.technologies = t.recordsets[0];
  //       console.log('Tech Data =>>', this.technologies);
  //       this.department = t.recordsets[1];
  //     });
  // }

  getProject() {
    this.apiser.getData(environment.localurlms, 'getProject').then((t: any) => {
      console.log(t.recordset);
      this.project = t.recordset;
      console.log('Project data =>>', this.project);
      this.MyTableData = new MatTableDataSource();
      this.MyTableData.data = t.recordset;
      this.MyTableData.paginator = this.paginator;
      this.MyTableData.sort = this.sort;

    });
  }

  applyFilter(filterValue: string): void {
    this.MyTableData.filter = filterValue.trim().toLowerCase();
    }


  createProjectForm(): void {
    this.projectFormGroup = this._formBuilder.group({
      pcode: ['', Validators.required],
      as_tech_mgr: ['', Validators.required],
      Organization: ['', Validators.required],
      projectName: ['', Validators.required],
      prj_mgr: ['', Validators.required],
      technology: [''],
      department: ['', Validators.required],
      count: ['', Validators.required],
      positions: [''],
      country: [''],
      dmodel: [''],
      state: [''],
      city: [''],
      address: [''],
      isActive: [''],
      projectInfo: ['']

    });
  }


  editProjectdetails(value): void {
    this.porject_Data = value;
    console.log('porject_Data', this.porject_Data);

    this.showAddempform = true;
    this.showEmptable = false;
    this.addbutton = false;
    this.cancelbutton = true;
    this.update = true;
    this.saveID = false;
    this.setStateCityData();
    this.statusShow = this.porject_Data.id;
    const obj: any = {};
    obj.compnayId = sessionStorage.getItem('companyId');
    obj.id = this.porject_Data.id;
    // console.log("obj data");


    this.fdate = new Date(this.porject_Data.fdate);
    this.tdate = new Date(this.porject_Data.tdate);

    console.log('Assign-->', this.fdate + '' + this.tdate);
    this.apiser.getData(environment.localurlms, 'getProjectDetails', obj).then((t: any) => {
      console.log('getProjectDetails', t.recordset);
      this.projectdetails = t.recordset;

      if (this.projectdetails.length > 0) {
        this.tableShow = true;
        this.tableID = 1;
      } else {
        this.tableShow = false;
        this.tableID = 0;
      }

    });

    this.projectFormGroup = this._formBuilder.group({
      pcode: [this.porject_Data.porjectName, Validators.required],
      as_tech_mgr: [this.porject_Data.PRJ_TM_EmpId, Validators.required],
      Organization: [this.porject_Data.orgId, Validators.required],
      projectName: [this.porject_Data.porjectName, Validators.required],
      prj_mgr: [this.porject_Data.PRJ_PM_EmpId, Validators.required],
      technology: [''],
      department: [this.porject_Data.deptId, Validators.required],
      positions: [''],
      count: [0],
      address: [this.porject_Data.Addres],
      country: [this.porject_Data.country],
      dmodel: [this.porject_Data.deliveryId],
      state: [this.porject_Data.state],
      city: [this.porject_Data.city],
      isActive: [this.porject_Data.projectStatus],
      projectInfo: [this.porject_Data.ProjDescription],
    });

  }


  setStateCityData(): void {
    this.getAllCountryDetails();
    this.stateData = this.stateDataResult.filter(
      state => state.State_Country == this.porject_Data.country
    );

    this.cityData = this.cityDataResult.filter(
      city => city.City_State_Id == this.porject_Data.state
    );

  }

  getFromdate(fdt) {
    this.formdate = this.datePipe.transform(fdt, 'dd MMM yyyy');
    console.log(this.formdate);
  }

  getTodate(todt) {
    this.todate = this.datePipe.transform(todt, 'dd MMM yyyy');
    console.log('To Date =>', this.todate);
  }

  submit(value): void {
    this.submitted = true;
    this.firsFromtDate = new Date(this.fdate);
    this.firsFromtDate.setMinutes( this.firsFromtDate.getMinutes() + 480 );

    this.secondToDate = new Date(this.tdate);
    this.secondToDate.setMinutes( this.secondToDate.getMinutes() + 480 );

    if (this.projectFormGroup.invalid) {
      return;
    } else if (this.projectdetails.length <= 0) {
      swal('Please Add atleast one technology and level');
    } else {
      const formData = {
        Organization: this.projectFormGroup.get('Organization').value,
        projectName: this.projectFormGroup.get('projectName').value,
        fdate: this.firsFromtDate,
        tdate: this.secondToDate,
        department: this.department_ID,
        mobile: '',
        email: '',
        address: this.projectFormGroup.get('address').value,
        projdescription: this.projectFormGroup.get('projectInfo').value,
        uid: this.userId,
        RecList: this.projectdetails,
        prj_pm_userid: this.ass_project_mgr_ID,
        prj_tm_userid: this.ass_tech_mgr_ID,
        company_Id: 1,
        country_Id: this.country_ID,
        state_id: this.state_ID,
        city_id: this.city_ID,
        deliveryModelId: this.deliverModel_ID,
        projectStatusId: this.projectStatus_ID
      };
      if((formData.fdate==null || formData.fdate=='') &&  (formData.tdate==null || formData.tdate=='')){
        formData.projectStatusId=4;
      }
      this.apiser
        .postData(environment.localurlms, 'ProjectInsert', formData)
        .then((t: any) => {
          // tslint:disable-next-line: triple-equals
          if (t.rowsAffected[0] == 1) {
            swal('', 'Data Inserted Successfully', 'success');
            this.projectFormGroup.reset();
            this.projectdetails = [];
            this.getProject();
            this.submitted = false;
            this.showEmptable = true;
            this.showAddempform = false;
            this.cancelbutton = false;
            this.addbutton = true;
            this.fdate = null;
            this.tdate = null;
          } else {
            swal('', 'Error While Insert', 'error');
          }
        })
        .catch(e => {
          console.log(e);
        });
    }
  }

  get f() {
    return this.projectFormGroup.controls;
  }

  adddata(value): void {
    console.log(this.skils);

    if (this.skils.techid == '' || this.skils.comptid == '') {
      swal('Error', 'Please Select Technology or Id', 'error');
    } else {
      console.log(this.projectFormGroup.value.count);
      this.skils.count=this.projectFormGroup.value.count;
      const myObjStr = this.skils;
      this.projectdetails.push(myObjStr);
      this.skils = {
        techid: '',
        comptid: ''
      };
      this.projectFormGroup.value.count=0;
      this.projectFormGroup.controls['technology'].reset();
      this.projectFormGroup.controls['positions'].reset();
      this.tableID = 1;
      this.tableShow = true;
    }

  }
  changecheck(val) {
    console.log(val);
  }
  removeparticular(i: any): void {
    this.projectdetails.splice(i, 1);
  }

  addNewemployee(): void {
    if (this.showEmptable == true) {
      this.showAddempform = true;
      this.showEmptable = false;
      this.addbutton = false;
      this.cancelbutton = true;
      this.update = false;
      this.saveID = true;
      this.projectdetails = [];
      this.tableShow = false;
      this.fdate = null;
      this.tdate = null;
    } else {
      this.showAddempform = false;
      this.showEmptable = true;
      this.addbutton = true;
      this.cancelbutton = false;
      this.update = false;
      this.saveID = true;
      this.projectdetails = [];
      this.tableShow = false;
      this.fdate = null;
      this.tdate = null;
    }
  }

  cancelAddemployee(): void {
    // tslint:disable-next-line: triple-equals
    this.getProject();
    if (this.showAddempform == true) {
      this.showEmptable = true;
      this.showAddempform = false;
      this.addbutton = true;
      this.cancelbutton = false;
      this.submitted = false;
      this.projectdetails = [];
      this.createProjectForm();
      this.fdate = null;
      this.tdate = null;
    } else {
      this.showEmptable = true;
      this.showAddempform = false;
      this.addbutton = false;
      this.cancelbutton = true;
      this.projectdetails = [];
      this.fdate = null;
      this.tdate = null;
    }
  }

  updateProject(value): void {
    // Projectupdate
    this.firsFromtDate = new Date(this.fdate);
    this.firsFromtDate.setMinutes( this.firsFromtDate.getMinutes() + 480 );

    this.secondToDate = new Date(this.tdate);
    this.secondToDate.setMinutes( this.secondToDate.getMinutes() + 480 );


    const formData = {
      orgId: this.projectFormGroup.get('Organization').value,
      projectName: this.projectFormGroup.get('projectName').value,
      fdate: this.firsFromtDate,
      tdate: this.secondToDate,
      department: this.projectFormGroup.get('department').value,
      mobile: '',
      email: '',
      address: this.projectFormGroup.get('address').value,
      projdescription: this.projectFormGroup.get('projectInfo').value,
      uid: this.userId,
      RecList: this.projectdetails,
      prj_pm_userid: this.projectFormGroup.get('prj_mgr').value,
      prj_tm_userid: this.projectFormGroup.get('as_tech_mgr').value,
      id: this.porject_Data.id,
      companyId: this.company_ID,
      countryId: this.projectFormGroup.get('country').value,
      stateId: this.projectFormGroup.get('state').value,
      cityId: this.projectFormGroup.get('city').value,
      deliveryModelId: this.projectFormGroup.get('dmodel').value,
      projectStatusId: this.projectStatus_ID,
    };

    if((formData.fdate==null || formData.fdate==null) &&  (formData.tdate==null || formData.tdate==null)){
      formData.projectStatusId=4;
    }

    console.log('FormData =>>>>>>', formData);
    this.apiser
      .postData(environment.localurlms, 'Projectupdate', formData)
      .then((t: any) => {
        console.log('Updated Data =>>>>>>>>>', t);
        swal('', 'Project Details Update Successfully', 'success');
        this.showAddempform = false;
        this.showEmptable = true;
        this.addbutton = true;
        this.cancelbutton = false;
        this.fdate = null;
      this.tdate = null;
        this.getProject();
      })
      .catch(e => {
        console.log(e);
      });
  }
}
