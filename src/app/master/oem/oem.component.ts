import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { AdddepartmentComponent } from '../../shared/adddepartment/adddepartment.component';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';
import { ApiService } from '../../service/api-service';
import { environment } from '../../../environments/environment';
import { CustomDataSource } from '../../shared/model/data-source';
import swal from 'sweetalert';

@Component({
  selector: 'app-oem',
  templateUrl: './oem.component.html',
  styleUrls: ['./oem.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class OemComponent implements OnInit {

  MatTableData: any;
  displayedColumns: string[] = ['id', 'oname', 'description', 'action','status'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService) { }

  ngOnInit(): void {

    this.getAllOEMDetails();
  }

  getAllOEMDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getOEM')
      .then((data: any) => {
        this.MatTableData = new MatTableDataSource();
        this.MatTableData.data = data.recordset;
        this.MatTableData.paginator = this.paginator;
        this.MatTableData.sort = this.sort;
      });
  }

  addOemopenDialog(): void {
    const dialogName = 'Add New OEM';
    const dialogId = 4;
    const diaglogRef = this.dialog.open(AdddepartmentComponent, {
      data: {
        dialogName,
        dialogId
      }
    });

    diaglogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllOEMDetails();
    });

  }
  
  statuschange(usr):void{
    let data:any={};
    data.id=usr.OEM_ID;
    data.companyId=sessionStorage.getItem('companyId');
    data.status=usr.IsActive;
    this.apiser.postData(environment.localurlms, 'updateOEMtatus', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfull', 'success');
        this.getAllOEMDetails();
        } else {
        swal('', 'Error While update', 'error');
      }
    })
  }

  editOEMopenDialog(user): void {
    const dialogName = 'Update OEM';
    const dialogId = 4;
    const DEPT_ID = 44;
    const dialogRef = this.dialog.open(AdddepartmentComponent, {
      data: { user,
        dialogName,
        dialogId,
        DEPT_ID
      }

    });

    dialogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllOEMDetails();
    });
  }

  confirmOpenDialog(): void {
    const label = 'Are you sure confirm delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
  }


  }


