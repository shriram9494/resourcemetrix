import { ApiService } from './../../service/api-service';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { trigger, style, transition, state, animate } from '@angular/animations';
import { AddrolepopupComponent } from '../../shared/addrolepopup/addrolepopup.component';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ]
})
export class RoleComponent implements OnInit {

  rollData: any = [];
  displayedColumns: string[] = ['id', 'rolename', 'roledesc', 'action'];
  MyTableData: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog, private apiser: ApiService) { }

  ngOnInit(): void {

    this.getAllRoleData();
  }

  getAllRoleData(): void {
    this.apiser
      .getData(environment.localurlms, 'getRoles')
      .then((data: any) => {
        console.log('Role data =>>>>', data.recordset);
        this.rollData = data.recordset;
        this.MyTableData = new MatTableDataSource();
        this.MyTableData.data = data.recordset;
        this.MyTableData.paginator = this.paginator;
        this.MyTableData.sort = this.sort;
      });
  }

  addRoleopenDialog(): void {
    const dialogName = 'Add New Role';

    const diaglogRef = this.dialog.open(AddrolepopupComponent, {
      data: {
        dialogName
      }
    });

    diaglogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllRoleData();
    });

  }

  editRoleopenDialog(roll): void {
    const dialogName = 'Update Role';
    const dialogRef = this.dialog.open(AddrolepopupComponent, {
      data: { roll,
        dialogName
      }

    });

    dialogRef.componentInstance.onAdd.subscribe((response) => {
      this.getAllRoleData();
    });
  }


}
