import { map, filter } from 'rxjs/operators';
import { ApiService } from './../../service/api-service';
import { Component, OnInit, ViewChild, ValueProvider } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatStepper,
  MatDialog,
  MatSelect,
  MatSort
} from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { ConfirmdialogComponent } from '../../shared/confirmdialog/confirmdialog.component';

import { EmployeeService } from '../../shared/services/employee.service';

// import { EmployeeService } from '../../shared/services/employee.service';
// import { Apiresponse } from '../../shared/model/apiresponse';

import { environment } from '../../../environments/environment';
import swal from 'sweetalert';
import { Employee } from '../../shared/model/employee';
import { CustomDataSource } from '../../shared/model/data-source';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

export interface Department {
  id: string;
  Name1: string;
}

export interface Technology {
  id: string;
  Name1: string;
  ProductName: string;
}

export interface Designation {
  id: string;
  Name1: string;
}

export interface Position {
  value: string;
  psoitionname: string;
}

export interface TestObject {
  technology: string;
  position: string;
}

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ opacity: '2' })),
      transition('void => *', [style({ opacity: '0.2' }), animate(555)]),
      transition('* => void', [style({ opacity: '2' })])
    ])
  ],
  providers: [ApiService]
})
export class EmployeeComponent implements OnInit {
  showEmptable: boolean = true;
  showAddempform: boolean = false;
  addbutton: boolean = true;
  cancelbutton: boolean = false;
  isLinear: boolean = false;
  personalFormGroup: FormGroup;
  technologyFormGroup: FormGroup;
  name: string;
  submitted: boolean = false;
  secondStep: boolean = false;
  empdata: any;
  empData: any;
  userID: number;
  uid: number;
  newFormArray: any[];
  emp: number;
  currentEmpID: number = 0;
  showtechTable: boolean = false;
  user: Employee;
  fname: any;
  department_ID: any;
  designation_ID: any;
  employeeData: any;
  buttonId: number = 0;
  country_ID: any;
  state_ID: any;
  city_ID: any;
  showError: boolean = false;
  chekEmail: any = [];
  emailList: any = [];
  company_ID: any;
  role_ID: any;
  disable: boolean = false;
  stateData: any;
  stateDataResult: any;
  cityData: any;
  cityDataResult: any;
  countryData: any;
  compitanceData: any = [];
  statusID: number = 0;
  technologyID: number = 0;
  saveButton: boolean = false;
  updateButton: boolean = false;
  totalRecords: number;
  rolesData: any = [];
  showFilter: boolean = true;
  report_MGRID: any;
  skils: any = {
    techid: '',
    productid: '',
    comptid: ''
  };

  isActiveId: any;
// tslint:disable-next-line: typedef
  isEditable = false;

  cancelButton: string = 'Cancel';
  // buttonName: string = 'Save And Next';
  MyDataSource: any;

  displayedColumns: string[] = [
    'id',
    'name',
    'email',
    'dep',
    'Technology',
    'Product',
    'Level',
    'des',
    'action',
    'status'
  ];

  displayedColumns2: string[] = ['id', 'technology', 'position', 'action'];
  // tslint:disable-next-line: typedef
  // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild('stepper') stepper;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private _formBuilder: FormBuilder,
    private dialog: MatDialog,
    private employeService: EmployeeService,
    private apiser: ApiService
  ) {
    this.userID = JSON.parse(sessionStorage.getItem('usrID'));
    this.company_ID = JSON.parse(sessionStorage.getItem('companyId'));
    console.log('company_ID', this.company_ID  + '' +  this.userID);
  }


// tslint:disable-next-line: member-ordering
  departments: Department[] = [];
// tslint:disable-next-line: member-ordering
  designation: Designation[] = [];
// tslint:disable-next-line: member-ordering
  technologies: Technology[] = [];

  productData: any = [];

// tslint:disable-next-line: member-ordering


  ngOnInit(): void {

    this.createEmployeForm();
    this.createTechnologyForm();
    this.getAllEmpolyeDetails();
    this.getTechdep();
    this.getAllCountryDetails();
    this.getAllExpertiesDetails();
    this.stringArr = [];

  }

  applyFilter(filterValue: string): void {
  this.MyDataSource.filter = filterValue.trim().toLowerCase();
  }


  getTechdep(): void {
    this.apiser.getData(environment.localurlms, 'techdepdetaisl').then((t: any) => {
      console.log("tasd", t);
        this.technologies = t.recordsets[0];
        this.departments = t.recordsets[1];
        this.designation = t.recordsets[2];
        this.rolesData = t.recordsets[3];
        this.productData = t.recordsets[5];
        let logRoleid=sessionStorage.getItem('roleId');
        if(logRoleid!='1'){
            this.rolesData = this.rolesData.filter(d => d.RoleId!=1);
          //  console.log(this.rollData);
        }
    });
  }

  getAllExpertiesDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getExperties')
      .then((data: any) => {
        console.log('Competance =>>>>', data.recordset);
        this.compitanceData = data.recordset;
      });
  }


  cancelAddemployee(): void {
    this.buttonId = 0;
    this.createEmployeForm();
    this.createTechnologyForm();
    this.getAllEmpolyeDetails();
    this.updateButton = true;
    if (this.showAddempform == true) {
      this.showEmptable = true;
      this.showAddempform = false;
      this.addbutton = true;
      this.cancelbutton = false;
      this.submitted = false;
      this.statusID = 0;
      this.showFilter = true;

    } else {
      this.showEmptable = true;
      this.showAddempform = false;
      this.addbutton = false;
      this.cancelbutton = true;
      this.showFilter = true;
      this.statusID = 0;

    }
  }

  createEmployeForm(): void {
    this.personalFormGroup = this._formBuilder.group({
      fname: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z ]*$'),
          Validators.minLength(3),
          Validators.maxLength(30)
        ])
      ],
      lname: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z ]*$'),
          Validators.minLength(3),
          Validators.maxLength(30)
        ])
      ],
      mobile: [
        '',
        Validators.required
      ],
      dob: [''],
      email: ['', Validators.required],
      department: ['', Validators.required],
      designation: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      address: [''],
      isActive: [''],
      role: [''],
      isUser: [''],
      rmanager: ['']
    });
  }

  createTechnologyForm(): void {
    this.technologyFormGroup = this._formBuilder.group({
      technology: [''],
      product: ['', Validators.required],
      position: ['', Validators.required]
    });
  }

  get f() {
    return this.personalFormGroup.controls;
  }

  addNewemployee(): void {
    // tslint:disable-next-line: triple-equals
    this.buttonId = 0;
    this.createEmployeForm();
    this.createTechnologyForm();
    if (this.showEmptable == true) {
      this.showAddempform = true;
      this.showEmptable = false;
      this.addbutton = false;
      this.cancelbutton = true;
      this.stringArr = [];
      this.showtechTable = false;
      this.showError = false;
      this.showFilter = false;
      this.updateButton = false;
      this.saveButton = false;

    } else {
      this.showAddempform = false;
      this.showEmptable = true;
      this.addbutton = true;
      this.cancelbutton = false;
      this.stringArr = [];
      this.showtechTable = false;
      this.showError = false;
      this.showFilter = false;
      this.updateButton = false;
      this.saveButton = false;
    }
  }

  getAllEmpolyeDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'getEmployee')
      .then((data: any) => {
        console.log('EMployesssData - >>', data);
        this.MyDataSource = new MatTableDataSource();
        this.empData = data.recordset;
        this.MyDataSource.data = data.recordset;
        this.MyDataSource.paginator = this.paginator;
        this.MyDataSource.sort = this.sort;

        this.getTechdep();
      });
  }

  // queryData:any;
  // getServer(event) {
  //   this.queryData.limit = event.pageSize;
  //   this.queryData.offset = event.pageSize * event.pageIndex;
  //   this.getAllEmpolyeDetails();
  // }

  getAllCountryDetails(): void {
    this.apiser
      .getData(environment.localurlms, 'Get_Contry_State_City')
      .then((data: any) => {
        // this.empData = data.recordset;
        this.stateDataResult = data.recordsets[0];
        this.countryData = data.recordsets[1];
        this.cityDataResult = data.recordsets[2];
        console.log('country --- = +',  this.countryData);


      });
  }

  onDepartmentSelection(value): void {
    this.department_ID = value;
  }

  onDesignationSelection(value): void {
    this.designation_ID = value;

  }


  onRoleSelection(value): void {
    this.role_ID = value;
    console.log('RoleID', this.role_ID);
  }


  onReportMgrSelection(value): void {
    this.report_MGRID = value;
    console.log('RoleID', this.report_MGRID);
  }


  onCountrySelection(value): void {
    this.getAllCountryDetails();
    this.country_ID = value;
    this.stateData = this.stateDataResult.filter(state=> state.State_Country ==  this.country_ID);
  }


  onStateSelection(value): void {
    this.state_ID = value;
    this.cityData=this.cityDataResult.filter(city=>city.City_State_Id ==  this.state_ID);
  }





  onCitySelection(value): void {
    this.city_ID = value;

  }

  checkEmail(event): void {
    let count = this.emailList.findIndex(x => x.toLowerCase() == event.toLowerCase());
      if  (count !=-1 ) {
        this.showError = true;
        this.disable = true;
      } else {
      this.showError = false;
      this.disable = false;
    }

  }

  statuschange(usr: any): void {
    const data: any = {};
    data.id = usr.id;
    data.companyId = sessionStorage.getItem('companyId');
    data.status = usr.EM_Status;
    console.log(data);
    this.apiser.postData(environment.localurlms, 'Update_EM_Status', data).then((t: any) => {
      // tslint:disable-next-line: triple-equals
      if (t.rowsAffected[0] == 1) {
        swal('', 'Status update sucessfull', 'success');
        this.getAllEmpolyeDetails();
        } else {
        swal('', 'Error While update', 'error');
      }
    });
  }

  submit(data, stepper: MatStepper): void {

    this.submitted = true;
    if (this.personalFormGroup.invalid) {
      stepper.selectedIndex = 0;
    } else {
      const formData = {
        fname: this.personalFormGroup.get('fname').value,
        lname: this.personalFormGroup.get('lname').value,
        mobile: this.personalFormGroup.get('mobile').value,
        dob: this.personalFormGroup.get('dob').value,
        email: this.personalFormGroup.get('email').value,
        department: this.department_ID,
        designation: this.designation_ID,
        companyId: this.company_ID,
        countryId: this.country_ID,
        stateId: this.state_ID,
        isUser: this.personalFormGroup.get('isUser').value,
        cityId: this.city_ID,
        roleId: this.role_ID,
        // report manager id
        reportId: this.report_MGRID,
        address: this.personalFormGroup.get('address').value,
        uid: sessionStorage.getItem('usrID')
      };
console.log('formData', formData);

      this.apiser
        .postData(environment.localurlms, 'EmployeeInsert', formData)
        .then((value: any) => {
          console.log('value=>>', value);
          this.currentEmpID = value.recordset[0].EM_ID;

          if (value) {
            this.getAllEmpolyeDetails();

            this.isEditable = true;
            this.currentEmpID = value.recordset[0].EM_ID;
            stepper.selectedIndex = 1;
            this.createEmployeForm();
            this.showFilter = true;
            this.updateButton = false;
            swal('', 'Employee Inserted Successfully!', 'success');

          } else {
            stepper.selectedIndex = 0;
          }
        });
    }
  }


  onStatusSelection(value): void {
    this.isActiveId = value;
    console.log('this.isActiveIdm', this.isActiveId);

  }


  // tslint:disable-next-line: member-ordering
  stringArr = [];

  addmoreTechnology(): void {

    // if(this.skils.techid =='' || this.skils.comptid =='' ){
    if(this.skils.productid =='' || this.skils.comptid =='' ){
      swal('Error', 'Please Select Technology or Id', 'error');
    } else {
      const myObjStr = this.skils;
      this.stringArr.push(myObjStr);
      this.skils = {
        techid: '',
        productid: '',
        comptid: ''
      };
      console.log('stringArr---------------->', this.stringArr);
      if (this.stringArr.length > 0) {
        this.showtechTable = true;
        this.saveButton = true;
      } else {
        this.showtechTable = false;
        this.saveButton = false;
      }
      this.createTechnologyForm();
    }

  }

  removeItem(i): void {
    this.stringArr.splice(i, 1);

    if (this.stringArr.length > 0) {
      this.saveButton = true;
      this.showtechTable = true;
    } else {
      this.saveButton = false;
      this.showtechTable = false;
    }
  }


  submitTech(data: any, stepper: MatStepper): void {
    let finaldata: any = {};
    let techdata: any = [];

    if (!this.currentEmpID) {
      swal('', 'Please Add New Employee', 'error');
      stepper.selectedIndex = 0;
      this.createEmployeForm();
      this.stringArr = [];
      this.showtechTable = false;
      this.saveButton = false;
      this.updateButton = false;
      return;
    }

    techdata = this.stringArr;
    finaldata.uid = this.userID;
    finaldata.emp = this.currentEmpID;
    finaldata.companyId = this.company_ID;
    finaldata.RecQtyList = techdata;

    this.apiser
      .postData(environment.localurlms, 'EmployeeTechnologyInsert', finaldata)
      .then((value: any) => {
        if (value) {
          this.createEmployeForm();
          finaldata = {};
          this.stringArr = [];
          this.showtechTable = false;
          this.cancelbutton = false;
          this.addbutton = true;
          this.saveButton = false;
          this.updateButton = false;
          // this.cancelButton = 'Go To List';
          this.getAllEmpolyeDetails();
          this.showEmptable = true;
          this.showAddempform = false;
          this.showFilter = true;
          swal('', 'Inserted Succefully!', 'success');
        } else {
          // stepper.selectedIndex = 1;
        }
      });
  }

  resetStepper(stepper: MatStepper): void {

    stepper.selectedIndex = 0;
    // tslint:disable-next-line: triple-equals
    if (stepper.selectedIndex == 0) {
      this.createEmployeForm();
      this.createTechnologyForm();
    } else {
    }
  }

// tslint:disable-next-line: member-ordering
  tech_comp_data: any = [];

  editEmpdetails(user: any, stepper: MatStepper): void {
    this.employeeData = user;
    console.log('update data =>>', this.employeeData);
    console.log('iiiddd =>>', this.company_ID);

    this.buttonId = this.employeeData.id;
    this.cancelbutton = true;
    this.addbutton = false;
    this.showAddempform = true;
    this.showEmptable = false;
    this.isEditable = true;
    this.statusID = this.employeeData.id;
    this.saveButton = false;
    this.showError = false;
    this.showFilter = false;
    this.updateButton = true;

    const techObj = {
       company_Id: this.company_ID,
       empid: this.employeeData.id
    };

    this.apiser
    .getData(environment.localurlms, 'getEmployeeDetails', techObj)
    .then((data: any) => {
      this.tech_comp_data = data.recordset;
      console.log('data -----', this.tech_comp_data);

      this.stringArr = this.tech_comp_data;
      // this.technologyID = this.tech_comp_data[0].techid;
      if ( this.stringArr.length > 0) {
          this.showtechTable = true;
          // this.updateButton = true;
      } else {
        this.showtechTable = false;
        // this.updateButton = false;
        this.stringArr = [];
      }
    });

    // console.log('technologyID', this.technologyID);


    this.setStateCountryData();
    const desiId = JSON.parse(this.employeeData.des_id);
    const depID = JSON.parse(this.employeeData.dep_id);
    this.employeeData.active == true ? this.employeeData.active = 1 : this.employeeData.active = 0;

    this.personalFormGroup.controls['fname'].setValue(this.employeeData.fname, { onlySelf: true });
    this.personalFormGroup.controls['lname'].setValue(this.employeeData.lname, { onlySelf: true });
    this.personalFormGroup.controls['email'].setValue(this.employeeData.email, { onlySelf: true });
    this.personalFormGroup.controls['designation'].setValue(desiId, { onlySelf: false });
    this.personalFormGroup.controls['department'].setValue(depID, { onlySelf: false });
    this.personalFormGroup.controls['mobile'].setValue(this.employeeData.mob, { onlySelf: true });
    this.personalFormGroup.controls['city'].setValue(this.employeeData.cityId, { onlySelf: true });
    this.personalFormGroup.controls['country'].setValue(this.employeeData.countryId, { onlySelf: true });
    this.personalFormGroup.controls['state'].setValue(this.employeeData.stateId, { onlySelf: false });
    this.personalFormGroup.controls['city'].setValue(this.employeeData.cityId, { onlySelf: false });
    this.personalFormGroup.controls['role'].setValue(this.employeeData.roleid, { onlySelf: false });
    this.personalFormGroup.controls['address'].setValue(this.employeeData.addres, { onlySelf: false });
    this.personalFormGroup.controls['isActive'].setValue(this.employeeData.active, { onlySelf: false });
    this.personalFormGroup.controls['rmanager'].setValue(this.employeeData.reportid, { onlySelf: false });
    // this.personalFormGroup.controls['isUser'].setValue(this.employeeData.isUser, { onlySelf: false });

  }

  setStateCountryData(){
    this.stateData = this.stateDataResult.filter(state=> state.State_Country ==  this.employeeData.countryId);
    this.cityData = this.cityDataResult.filter(city=>city.City_State_Id ==  this.employeeData.stateId);
    this.getAllCountryDetails();
  }


  updateEmployee(data, stepper: MatStepper): void {
    console.log(data, stepper);

    const formData = {
      fname: this.personalFormGroup.get('fname').value,
      lname: this.personalFormGroup.get('lname').value,
      mobile: this.personalFormGroup.get('mobile').value,
      dob: this.personalFormGroup.get('dob').value,
      email: this.personalFormGroup.get('email').value,
      department: this.personalFormGroup.get('department').value,
      designation: this.personalFormGroup.get('designation').value,
      companyId: this.company_ID,
      countryId: this.personalFormGroup.get('country').value,
      stateId: this.personalFormGroup.get('state').value,
      cityId: this.personalFormGroup.get('city').value,
      address: this.personalFormGroup.get('address').value,
      isUser: this.personalFormGroup.get('isUser').value,
      isActive: this.isActiveId,
      id: this.employeeData.id,
      uid: this.userID,
      roleId: this.personalFormGroup.get('role').value,
      reportId: this.personalFormGroup.get('rmanager').value
    };


    this.apiser
    .postData(environment.localurlms, 'EmployeeUpdate', formData)
    .then((value: any) => {
      console.log('value=>>', value);
        this.getAllEmpolyeDetails();
        this.isEditable = true;
        stepper.selectedIndex = 1;
        this.showAddempform = false;
        this.showEmptable = true;
        this.addbutton = true;
        this.cancelbutton = false;
        this.showError = false;
        this.showFilter = true;
        this.saveButton = false;
        this.updateButton = false;

        swal('', 'Employee Update Successfully!', 'success');


    });

  }

  updateTech(data: any, stepper: MatSelect): void {



      const formData = {
        id: this.employeeData.id,
        uid: this.userID,
        RecQtyList: this.stringArr,
        companyId: this.company_ID
      };

      console.log('formData ---------', formData);

    this.apiser
    .postData(environment.localurlms, 'EmployeeTechnologyupdate', formData)
    .then((value: any) => {
      console.log('value=>>', value);
        swal('', 'Technology And Position Update Successfully!', 'success');
        this.getAllEmpolyeDetails();
        this.showAddempform = false;
        this.showEmptable = true;
        this.addbutton = true;
        this.cancelbutton = false;
        this.showFilter = true;
        this.saveButton = false;
        this.updateButton = false;
        // stepper.selectedIndex = 0;

    });


  }

  confirmOpenDialog(): void {
    const label = 'Are you sure confirm delete this item';
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      data: {
        label
      }
    });
  }


}
